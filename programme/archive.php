<?php $presenters = sch_get_presenters ("confirmed"); ?>
<?php $materials = sch_get_all_materials ();  ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">

	    <?php if (isset($_POST['resend_email'])) { ?>
		<div class="five-second-removal">
		    <?php if (sch_resend_archive_email($_POST['resend_email'])) { ?>
			<div class="alert alert-success" role="alert">
			    If the email provided matches a presenter's email, they have been emailed with a link to edit their entry on the archival version of the conference programme
			</div>
		    <?php } else { ?>
			<div class="alert alert-danger" role="alert">
			    Database error when checking for presenter email
			</div>
		    <?php } ?>
		</div>
	    <?php } ?>
	    
	    <h4>Conference programme</h4>

	    <div class="alert alert-warning" role="alert">
		This conference has already completed and presentations can no longer be joined; the following is provided for archival purposes only
	    </div>
	    
	    <?php
	    
	    // Read the time zone data into memory
	    $tzfile = fopen(ABS_PATH . "timezones.csv", "r");
	    $timezones = [];
	    while (! feof($tzfile)) {
		$timezones[] = fgetcsv($tzfile);
	    }
	    fclose($tzfile);

	    ?>
	    
	    <div class="form-group mb-3">
		<label for="partic_signup_tz_selector">Display times in another time zone</label>
		<select class="form-control form-control-sm" id="partic_signup_tz_selector">
		    <option value="0">UTC</option>
		    <?php

		    // The variable $tz[2] in the loop below means that we take
		    // daylight savings time; switch to $tz[1] for standard time
		    foreach ($timezones as $tz) {
			if ($tz[0] != "timezone" & $tz[0] != "UTC") {
			    echo '<option value="' . $tz[2] . '">';
			    echo $tz[0];
			    echo '</option>';
			}
		    }
		    
		    ?>
		</select>
	    </div>

	    <div class="table-responsive">
		<table class="table table-striped">
		    <thead>
			<tr>
			    <td scope="col">Details</td>
			    <td scope="col" style="text-align: right;">Time</td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $presenter) { if ($presenter['hidden_from_archive_admin'] == 0 & $presenter['hidden_from_archive_presenter'] == 0) { ?>
			    <?php

			    $utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60) . " UTC";
			    
			    if ($presenter['pronouns'] != "") {
				$pronouns = "(" . $presenter['pronouns'] . ")";
			    } else {
				$pronouns = "";
			    }

			    ?>
			    <tr>
				<td>
				    <label class="form-check-label" for="partic_presenter_<?php echo $presenter['id']; ?>">
					<h5><?php echo $presenter['title']; ?></h5>
				    </label>
				    <h6 class="text-muted">by <?php echo $presenter['name']; ?> <?php echo $pronouns; ?></h6>
				    <h6 class="text-muted mb-3"><?php echo $presenter['handle']; ?></h6>
				    <p><?php echo sch_format_text($presenter['abstract'], TRUE); ?></p>
				    <?php foreach ($materials as $mat) { if ($mat['presenter'] == $presenter['id']) { ?>
					<?php if ($mat['materials_type'] == "link") { ?>
					    <?php if ($mat['materials_title'] == "") {$mat['materials_title'] = "Link";} ?>
					    <p><img src="<?php echo SITE_URL ?>images/link-45deg.svg"> <a href="<?php echo sch_sanitize_text($mat['materials_content']); ?>" target="_blank"><?php echo sch_sanitize_text($mat['materials_title']); ?></a></p>
					<?php } ?>
					<?php if ($mat['materials_type'] == "file") { ?>
					    <?php if ($mat['materials_title'] == "") {$mat['materials_title'] = "File";} ?>
					    <?php $presenter_dir = substr(md5($presenter['id']), 0, 12); ?>
					    <p><img src="<?php echo SITE_URL ?>images/file-earmark-arrow-down.svg"> <a href="<?php echo SITE_URL; ?>archive/files/<?php echo $presenter_dir; ?>/<?php echo sch_sanitize_text($mat['materials_content']); ?>"><?php echo sch_sanitize_text($mat['materials_title']); ?></a></p>
					<?php } ?>
					<?php if ($mat['materials_type'] == "text") { ?>
					    <?php if ($mat['materials_title'] == "") {$mat['materials_title'] = "Text";} ?>
					    <p><img src="<?php echo SITE_URL ?>images/journal-text.svg"> <a href="#" onclick="event.preventDefault();$(this).parent().next().slideToggle();"><?php echo sch_sanitize_text($mat['materials_title']); ?></a></p>
					    <div class="card mb-3" style="display: none;">
						<div class="card-body">
						    <p><?php echo sch_sanitize_text($mat['materials_content'], TRUE); ?></p>
						    <small><a href="#" onclick="event.preventDefault();$(this).parent().parent().parent().slideUp();">[Close text]</a></small>
						</div>
					    </div>
					<?php } ?>
				    <?php } } ?>
				</td>
				<td style="text-align: right;">
				    <p><span class="signup_time" data-slot="<?php echo $presenter['confirmed_slot']; ?>"><?php echo $utc_time; ?></span></p>
				</td>
			    </tr>
			<?php } } ?>
		    </tbody>
		</table>
	    </div>

	    <div id="request-link-show" onclick="event.preventDefault();$('#request-link').slideDown();$('#request-link-show').slideUp();"><a href="#"><small>Resend link to upload presentation materials and edit presenter details</small></a></div>

	    <div id="request-link" style="display:none">
		<h4>Resend link to upload presentation materials and edit presenter details</h4>
		<p>The following is for the use of presenters who would like to add presentation materials, edit or remove their entry on the conference's archived programme.</p>
		<p>Enter the email address you provided to <?php echo CONF_NAME; ?> when you signed up to present in order to receive a link where you can upload your presentation materials, hide or delete your presentation from the archived programme.</p>
		<form action="<?php echo SITE_URL; ?>programme/" method="post">
		    <div class="form-group mb-3">
			<label for="resend_email">Your email</label>
			<input type="email" class="form-control" id="resend_email" name="resend_email" placeholder="yourname@domain.muffin" value="">
			<small id="presenter_email_help" class="form-text text-muted">
			    Enter the email address that you provided to <?php echo CONF_NAME; ?> when you signed up to present
			</small>
		    </div>
		    <button class="btn btn-lg btn-primary">Send link</button>
		    <button class="btn btn-lg btn-secondary" onclick="event.preventDefault();$('#request-link').slideUp();$('#request-link-show').slideDown();">Cancel</button>
		</form>
	    </div>
	</div>
    </div>
</div>
