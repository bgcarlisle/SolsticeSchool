<?php

include_once ("../config.php");

$presenter = sch_get_presenter ($_GET['presenter']);

$filename = substr(preg_replace("/[^a-zA-Z0-9]/", "_", $presenter['title']), 0, 20) . ".ics";
$filename = preg_replace("/[_]+/", "_", $filename);

header('Content-Type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename);

$utc_time_start = date("Ymd\THis", strtotime(CONF_START) + ($presenter['assigned_slot'] - 1) * 60 * 60);

$utc_time_end = date("Ymd\THis", strtotime(CONF_START) + ($presenter['assigned_slot'] - 1) * 60 * 60 + 60 * 60);

?>BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//summerschool.scholar.social//Summer School Scheduler 1.0//EN
CALSCALE:GREGORIAN
METHOD:PUBLISH
BEGIN:VEVENT
SUMMARY:'<?php echo $presenter['title']; ?>' (<?php echo CONF_NAME; ?>)
UID:<?php echo substr(md5(uniqid(rand(), true)) , 0, 8); ?>-<?php echo substr(md5(uniqid(rand(), true)) , 0, 4); ?>-<?php echo substr(md5(uniqid(rand(), true)) , 0, 4); ?>-<?php echo substr(md5(uniqid(rand(), true)) , 0, 4); ?>-<?php echo substr(md5(uniqid(rand(), true)) , 0, 12) . "\n"; ?>
SEQUENCE:0
STATUS:CONFIRMED
TRANSP:OPAQUE
DTSTART:<?php echo $utc_time_start . "Z\n"; ?>
DTEND:<?php echo $utc_time_end . "Z\n"; ?>
DTSTAMP:<?php echo date("Ymd\THis") . "Z\n"; ?>
DESCRIPTION:Presented by <?php echo $presenter['name'] . "\n"; ?>
URL:<?php echo SITE_URL . "\n"; ?>
END:VEVENT
END:VCALENDAR
