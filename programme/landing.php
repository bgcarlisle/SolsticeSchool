<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$link = sch_participant_link ($_GET['link']);

?>

<?php if ($link) { // Link is good ?>

    <?php

    $presenters = sch_get_presenters ("confirmed");

    $dbsignups = sch_get_participant_signups ($link['id']);

    $signups = array_column ($dbsignups, 'presenter');

    ?>

    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		<h3>Attendance plan for <?php echo $link['email']; ?></h3>
		<p>Thank you for signing up to participate in <?php echo CONF_NAME; ?>! Your email address has been verified. We will email you with a link to a web conference room for each of the presentations you have selected below on the day of the presentation.</p>

		<h4>Conference programme</h4>

		<p>Please select all the presentations that you would like to attend and participate in</p>

		<?php
		
		// Read the time zone data into memory
		$tzfile = fopen(ABS_PATH . "timezones.csv", "r");
		$timezones = [];
		while (! feof($tzfile)) {
		    $timezones[] = fgetcsv($tzfile);
		}
		fclose($tzfile);

		?>
		<div class="form-group mb-3">
		    <label for="partic_signup_tz_selector">Display times in another time zone</label>
		    <select class="form-control form-control-sm" id="partic_signup_tz_selector">
			<option value="0">UTC</option>
			<?php

			// The variable $tz[2] in the loop below means that we take
			// daylight savings time; switch to $tz[1] for standard time
			foreach ($timezones as $tz) {
			    if ($tz[0] != "timezone" & $tz[0] != "UTC") {
				echo '<option value="' . $tz[2] . '">';
				echo $tz[0];
				echo '</option>';
			    }
			}
			
			?>
		    </select>
		</div>

		<div class="table-responsive">
		    <table class="table table-striped">
			<thead>
			    <tr>
				<td scope="col">&nbsp;</td>
				<td scope="col">Details</td>
				<td scope="col" style="text-align: right;">Time</td>
			    </tr>
			</thead>
			<tbody>
			    <?php foreach ($presenters as $presenter) { ?>
				<?php

				$utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60) . " UTC";

				$past = time() > (strtotime(CONF_START) + ($presenter['confirmed_slot']) * 60 * 60);

				if ($presenter['pronouns'] != "") {
				    $pronouns = "(" . $presenter['pronouns'] . ")";
				} else {
				    $pronouns = "";
				}

				if (in_array($presenter['id'], $signups)) {
				    $present_checked = " checked";
				} else {
				    $present_checked = "";
				}

				?>
				<tr>
				    <td>
					<div class="form-check mx-3">
					    <input class="form-check-input partic-signup-time-checkbox" type="checkbox" value="TRUE" data-slot="<?php echo $presenter['id']; ?>" id="partic_presenter_<?php echo $presenter['id']; ?>"<?php echo $present_checked; ?>>
					</div>
				    </td>
				    <td>
					<label class="form-check-label" for="partic_presenter_<?php echo $presenter['id']; ?>">
					    <h5><?php echo $presenter['title']; ?></h5>
					</label>
					<h6 class="text-muted">by <?php echo $presenter['name']; ?> <?php echo $pronouns; ?></h6>
					<h6 class="text-muted mb-3"><?php echo $presenter['handle']; ?></h6>
					<p><?php echo sch_format_text($presenter['abstract'], TRUE); ?></p>
					<?php if ($past) { ?>
					    <div class="alert alert-warning" role="alert">
						This presentation has already completed and can no longer be joined
					    </div>
					<?php } ?>
				    </td>
				    <td style="text-align: right;">
					<p><span class="signup_time" data-slot="<?php echo $presenter['confirmed_slot']; ?>"><?php echo $utc_time; ?></span></p>
					<p><a href="<?php echo SITE_URL; ?>programme/calendar.php?presenter=<?php echo $presenter['id']; ?>">Download event as an .ics file</p>
				    </td>
				</tr>
			    <?php } ?>
			</tbody>
		    </table>
		</div>
		<p><a href="<?php echo SITE_URL; ?>programme/calendar-all.php">Download all presentations as an .ics file</a></p>
		<p id="download-checked-presentations-link"><a href="<?php echo SITE_URL; ?>programme/calendar-checked.php?presentations=<?php echo implode(",", $signups); ?>">Download checked presentations as an .ics file</a></p>
		<input type="hidden" id="checked_presentations" value="<?php echo implode(",", $signups); ?>">
		<input type="hidden" id="checked_presentations_saved" value="<?php echo implode(",", $signups); ?>">
		<input type="hidden" id="participant_id" value="<?php echo $link['id']; ?>">

		<button id="partic-save-changes-button" class="btn btn-lg btn-primary mb-3" disabled>Save changes</button>
		<div class="alert alert-success" role="alert" style="display: none;" id="partic-save-changes-feedback">Changes saved</div>
	    </div>
	</div>
    </div>

<?php } else { // Link is bad ?>

    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		
		<div class="alert alert-danger" role="alert">
		    Expired link, database error or bad link
		</div>

		<p>Enter your email to have a new link emailed to you</p>

		<div class="form-group mb-3">
		    <label for="partic_change_email">Your email</label>
		    <input type="email" class="form-control" id="partic_change_email" placeholder="yourname@domain.muffin">
		    <small id="partic_change_email_help" class="form-text text-muted">
			Enter the email you used to sign up previously; be sure to check your spam folder
		    </small>
		</div>
		<button class="btn btn-lg btn-primary" id="send-participant-change">Send email</button>

	    </div>
	</div>
    </div>


<?php } ?>

<?php

include (ABS_PATH . "footer.php");

?>
