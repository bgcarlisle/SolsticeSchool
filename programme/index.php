<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(PARTIC_SIGNUP_START)) {
    if (time() <= strtotime(CONF_END)) {
	include (ABS_PATH . "programme/signup.php");
    } else {
	include (ABS_PATH . "programme/archive.php");
    }
} else {
    include (ABS_PATH . "programme/no-signup.php");
}


include (ABS_PATH . "footer.php");

?>
