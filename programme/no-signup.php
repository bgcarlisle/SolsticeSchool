<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Participant signup</h3>
	    <p>This form is available starting <?php echo PARTIC_SIGNUP_START; ?>.</p>
	    <p><a href="<?php echo SITE_URL; ?>participants/">More info on participating</a></p>

	</div>
    </div>
</div>
