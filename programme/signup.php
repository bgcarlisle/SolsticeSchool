<?php $presenters = sch_get_presenters ("confirmed"); ?>
<?php $sent_invites = sch_get_sent_invite_emails (); ?>
<div id="action-mask"></div>
<div class="action-prompt" id="change-participant-signups-dialog">
    <button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
    <div id="send-participant-change-email-inner">
	<h4>View or change presentation attendance plans</h4>
	<p>If you have already saved your attendance plans for <?php echo CONF_NAME; ?>, enter the email you used to sign up and this will email a link where you can view or edit your attendance plan:</p>

	<div class="form-group mb-3">
	    <label for="partic_change_email">Your email</label>
	    <input type="email" class="form-control" id="partic_change_email" placeholder="yourname@domain.muffin">
	    <small id="partic_change_email_help" class="form-text text-muted">
		Enter the email you used to sign up previously; be sure to check your spam folder
	    </small>
	</div>
	<hr>
	<button class="btn btn-lg btn-success" id="send-participant-change">Send email</button>
	<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
    </div>
</div>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Participant signup</h3>
	    <p>Use the following form to sign up to attend and participate in a presentation at <?php echo CONF_NAME; ?>. This form will be available until 23:59 UTC on <?php echo CONF_END;  ?>.</p>
	    <p><a href="<?php echo SITE_URL; ?>participants/">More info on participating</a></p>

	    <button id="partic-change-signup-button" class="btn btn-sm btn-secondary">View or change saved attendance plans</button>

	    <hr>

	    <div id="participant-signup-form-container">
		
		<h4>How to contact you</h4>

		<p>On the day of each presentation, we will send an email to everyone who signed up to participate with the link to the video conference room.</p>
		
		<div class="form-group">
		    <label for="partic_email">Your email</label>
		    <input type="email" class="form-control" id="partic_email" placeholder="yourname@domain.egg">
		    <small id="partic_email_help" class="form-text text-muted">
			Use an email that you will be checking between <?php echo CONF_START; ?> and <?php echo CONF_END; ?>. The link to the video conference room will be sent out about 24 hours in advance. If you sign up later than that, please contact us directly and ask for the link via the <a href="<?php echo SITE_URL; ?>contact/">contact page</a>.
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="partic_handle">Your handle on the Fediverse</label>
		    <input type="text" class="form-control" id="partic_handle" placeholder="@SummerSchool@scholar.social">
		    <small id="partic_handle_help" class="form-text text-muted">
			We will only use this to contact you in the case of last-minute changes to the presentation venue or cancellation
		    </small>
		</div>

		<hr>

		<h4>Conference programme</h4>
		
		<?php if (time() <= strtotime(CONF_END)) { ?>

		    <p>Please select all the presentations that you would like to attend and participate in</p>
		    
		<?php } ?>

		<?php
		
		// Read the time zone data into memory
		$tzfile = fopen(ABS_PATH . "timezones.csv", "r");
		$timezones = [];
		while (! feof($tzfile)) {
		    $timezones[] = fgetcsv($tzfile);
		}
		fclose($tzfile);

		?>
		<div class="form-group mb-3">
		    <label for="partic_signup_tz_selector">Display times in another time zone</label>
		    <select class="form-control form-control-sm" id="partic_signup_tz_selector">
			<option value="0">UTC</option>
			<?php

			// The variable $tz[2] in the loop below means that we take
			// daylight savings time; switch to $tz[1] for standard time
			foreach ($timezones as $tz) {
			    if ($tz[0] != "timezone" & $tz[0] != "UTC") {
				echo '<option value="' . $tz[2] . '">';
				echo $tz[0];
				echo '</option>';
			    }
			}
			
			?>
		    </select>
		</div>

		<div class="table-responsive">
		    <table class="table table-striped">
			<thead>
			    <tr>
				<td scope="col">&nbsp;</td>
				<td scope="col">Details</td>
				<td scope="col" style="text-align: right;">Time</td>
			    </tr>
			</thead>
			<tbody>
			    <?php foreach ($presenters as $presenter) { ?>
				<?php

				$utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60) . " UTC";

				$past = time() > (strtotime(CONF_START) + ($presenter['confirmed_slot']) * 60 * 60);
				
				if ($presenter['pronouns'] != "") {
				    $pronouns = "(" . $presenter['pronouns'] . ")";
				} else {
				    $pronouns = "";
				}

				// Check whether invites have been sent
				$invite_sent = FALSE;
				foreach ($sent_invites as $si) {
				    if ($si['presenter'] == $presenter['id']) {
					$invite_sent = TRUE;
				    }
				}

				?>
				<tr>
				    <td>
					<div class="form-check mx-3">
					    <?php if (! $past && ! $invite_sent) { ?>
						<input class="form-check-input partic-signup-time-checkbox" type="checkbox" value="TRUE" data-slot="<?php echo $presenter['id']; ?>" id="partic_presenter_<?php echo $presenter['id']; ?>">
					    <?php } ?>
					</div>
				    </td>
				    <td>
					<label class="form-check-label" for="partic_presenter_<?php echo $presenter['id']; ?>">
					    <h5><?php echo $presenter['title']; ?></h5>
					</label>
					<h6 class="text-muted">by <?php echo $presenter['name']; ?> <?php echo $pronouns; ?></h6>
					<h6 class="text-muted mb-3"><?php echo $presenter['handle']; ?></h6>
					<p><?php echo sch_format_text($presenter['abstract'], TRUE); ?></p>
					<hr>
					<?php if ($past) { ?>
					    <div class="alert alert-warning" role="alert">
						This presentation has already completed and can no longer be joined
					    </div>
					<?php } else { ?>
					    <?php if ($presenter['able_to_host'] == 1) { ?>
					    <p>This presenter has opted to provide their own video-conferencing room, and so <?php echo CONF_NAME; ?> cannot guarantee that automatic closed captioning will be available.</p>
					<?php } else { ?>
					    <p><?php echo CONF_NAME; ?> will provide a video-conferencing room for this presentation and so automatic closed captioning will be available.</p>
					    <?php } ?>
					    <?php if ($invite_sent) { ?>
						<div class="alert alert-warning" role="alert">
						    We have already sent invitation emails out for this presentation; for last-minute admission, please contact us via direct message on Fedi (@SummerSchool@scholar.social) or use the <a href="<?php echo SITE_URL; ?>contact/">Contact page</a>
						</div>
					    <?php } ?>
					<?php } ?>
				    </td>
				    <td style="text-align: right;">
					<p><span class="signup_time" data-slot="<?php echo $presenter['confirmed_slot']; ?>"><?php echo $utc_time; ?></span></p>
					<p><a href="<?php echo SITE_URL; ?>programme/calendar.php?presenter=<?php echo $presenter['id']; ?>">Download event as an .ics file</p>
				    </td>
				</tr>
			    <?php } ?>
			</tbody>
		    </table>
		</div>
		<p><a href="<?php echo SITE_URL; ?>programme/calendar-all.php">Download all presentations as an .ics file</a></p>
		<p id="download-checked-presentations-link" style="display: none;"><a href="<?php echo SITE_URL; ?>programme/calendar-all.php">Download checked presentations as an .ics file</a></p>
		<input type="hidden" id="checked_presentations">

		<div class="form-check mb-3">
		    <input class="form-check-input" type="checkbox" value="TRUE" id="partic_policy">
		    <label class="form-check-label" for="partic_policy">
			I have read, understood and agree to the <a href="<?php echo SITE_URL; ?>participants/">expectations for participants</a> at <?php echo CONF_NAME; ?>
		    </label>
		</div>

		<p class="mb-3">Your email and Fediverse handle will not be published anywhere. We will clear the database of participant contact information on <?php echo CONF_END; ?>.</p>

		    <button id="partic-signup-button" class="btn btn-lg btn-primary" disabled>Save your attendance plans</button>

	    </div>

	</div>
    </div>
</div>
