<?php

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {
    $presenter_form_avail = TRUE;
} else {
    $presenter_form_avail = FALSE;
}

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {
    $mod_form_avail = TRUE;
} else {
    $mod_form_avail = FALSE;
}

?>
<div class="container">
    <div class="row">
	<div class="col-md-6">
	    <div class="card mb-3">
		<div class="card-body">
		    <h3 class="card-title">Presenters</h3>
		    <p>Prepare and present a 10-15 minute talk on a subject of your choice with time for discussion afterward.</p>
		    <a href="<?php echo SITE_URL; ?>presenters/" class="card-link">More info on presenting</a>
		    <?php if ($presenter_form_avail) { ?>
			<hr>
			<a href="<?php echo SITE_URL; ?>presenters-signup/" class="btn btn-primary">I would like to present</a>
		    <?php } ?>
		</div>
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="card mb-3">
		<div class="card-body">
		    <h3 class="card-title">Moderators</h3>
		    <p>Support presenters by introducing them, enforcing rules, and facilitating discussion afterward.</p>
		    <a href="<?php echo SITE_URL; ?>moderators/" class="card-link">More info on moderation</a>
		    <?php if ($mod_form_avail) { ?>
			<hr>
			<a href="<?php echo SITE_URL; ?>moderators-signup/" class="btn btn-primary">I would like to moderate</a>
		    <?php } ?>
		</div>
	    </div>
	</div>
    </div>
    <div class="row">
	<div class="col-md-12">
	    <div class="card mb-3">
		<div class="card-body">
		    <h3 class="card-title">Programme</h3>
		    <?php if (time() < strtotime(CONF_END)) { ?>
			<p>View the conference programme or sign up to attend a presentation and participate in the discussion afterward.</p>
		    <?php } else { ?>
			<p>View the archived conference programme.</p>

		    <?php } ?>
		    <a href="<?php echo SITE_URL; ?>participants/" class="card-link">More info on participation</a>
		    <hr>
		    <a href="<?php echo SITE_URL; ?>programme/" class="btn btn-primary">View programme</a>
		</div>
	    </div>
	</div>
    </div>
</div>
