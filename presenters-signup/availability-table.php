<?php

// WARNING:
// This table is used in both `signup.php` and `confirm.php`, so if
// you make changes to it with one in mind, check that it doesn't
// break the other page!

include_once ("../config.php");

// First, calculate the number of slots: 1 per hour, 24 per day
// starting at 0:00 on the first day of the conference and going until
// 23:00 on the last day of the conference.

$n_slots = (strtotime(CONF_END) - strtotime(CONF_START) + 86400)/60/60;

// echo "n slots: " . $n_slots . "<br>";

// Now find the date and hour of the first slot by adjusting the
// time of the first slot in UTC by the time zone offset.

$first_slot = strtotime(CONF_START) + $_POST['tzoffset'];
$last_slot = $first_slot + 60* 60 * ($n_slots - 1);
$n_leading_slots = date("H", $first_slot);

// echo "first slot: " . date("Y-m-d H:00", $first_slot) . "<br>";
// echo "last slot: " . date("Y-m-d H:00", $last_slot);

// Now make a table that starts on that date and that time, and
// continues for $n_slots slots.

?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-sm presenter-avail">
	<thead>
	    <tr>
		<td scope="col">Date</td>
		<?php

		$grid_hour = 0;

		while ($grid_hour < 24) {

		    echo '<td scope="col">' . $grid_hour . ':00</td>';

		    $grid_hour++;
		    
		}
		
		?>
	    </tr>
	</thead>
	<tbody>
	    <?php

	    $grid_date = strtotime(date("Y-m-d", $first_slot));

	    $slot = 1;

	    $leading_slot_counter = 0;

	    while ($grid_date <= strtotime(date("Y-m-d", $last_slot))) {

		echo '<tr>';

		echo '<td scope="row">' . date("Y-m-d", $grid_date) . '</td>';

		$grid_hour = 0;

		while ($grid_hour < 24) {

		    if ($leading_slot_counter < $n_leading_slots) {
			// Leading slots
			echo '<td></td>';
		    } else {

			if ($slot <= $n_slots) {
			    
			    echo '<td>';

			    echo '<input id="avail_check_' . $slot . '" class="form-check-input availability_check" type="checkbox" value="' . $slot . '">';

			    echo '</td>';
			    
			    $slot++;
			    
			} else {
			    // Trailing slots
			    echo '<td></td>';
			}
		    }

		    $leading_slot_counter++;
		    $grid_hour++;
		    
		}

		echo "</tr>";

		$grid_date += 86400;
		
	    }
	    
	    ?>
	</tbody>
    </table>
</div>
