<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Presenter signup</h3>
	    <p>Use the following form to sign up to present at <?php echo CONF_NAME; ?>. This form will be available until 23:59 UTC on <?php echo PRESENT_SIGNUP_END;  ?>.</p>
	    <p><a href="<?php echo SITE_URL; ?>presenters/">More info on presenting</a></p>

	    <hr>
	    
	    <div id="presenter_form">

		<h4>About you</h4>
		
		<div class="form-group">
		    <label for="presenter_name">Your name</label>
		    <input type="text" class="form-control" id="presenter_name" placeholder="E.g. Otter">
		    <small id="presenter_name_help" class="form-text text-muted">
			This doesn't need to be your legal name; use the name that you would like us to use to refer to you.
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="presenter_pronouns">Your pronouns</label>
		    <input type="text" class="form-control" id="presenter_pronouns" placeholder="E.g. they/them">
		    <small id="presenter_pronouns_help" class="form-text text-muted">
			Optional
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="presenter_handle">Your handle on the Fediverse</label>
		    <input type="text" class="form-control" id="presenter_handle" placeholder="@SummerSchool@scholar.social">
		    <small id="presenter_handle_help" class="form-text text-muted">
			Use the handle that you would like to be credited for your presentation
		    </small>
		</div>
		
		<div class="form-group mb-3">
		    <label for="presenter_email">Your email</label>
		    <input type="email" class="form-control" id="presenter_email" placeholder="yourname@domain.muffin">
		    <small id="presenter_email_help" class="form-text text-muted">
			Use an email address where you can be reached to confirm details of your presentation, like finalizing the date/time, matching you with a moderator, etc.
		    </small>
		</div>

		<hr>

		<h4>Your presentation</h4>
		
		<div class="form-group">
		    <label for="presenter_title">Title</label>
		    <input type="text" class="form-control" id="presenter_title" placeholder="Baking the best apple crumble to celebrate #MastodonCrumbles">
		    <small id="presenter_title_help" class="form-text text-muted">
			A short title for your presentation
		    </small>
		</div>

		<div class="form-group">
		    <label for="presenter_abstract">Abstract</label>
		    <textarea class="form-control" id="presenter_abstract" rows="8"></textarea>
		    <small id="presenter_title_help" class="form-text text-muted">
			Aim for 500 words or less
		    </small>
		</div>

		<hr>
		
		<p><?php echo CONF_NAME; ?> will provide a video conference room for your presentation that includes automatic captioning to aid in accessibility, unless you prefer to host your own presentation.</p>

		<div class="form-check">
		    <input class="form-check-input" type="checkbox" value="TRUE" id="presenter_host">
		    <label class="form-check-label" for="presenter_host">
			I prefer to host my own presentation by providing a publicly available video conference room
		    </label>
		</div>

		<div class="form-check">
		    <input class="form-check-input" type="checkbox" value="TRUE" id="presenter_recording">
		    <label class="form-check-label" for="presenter_recording">
			I would like to have my presentation recorded
		    </label>
		</div>

		<div class="form-check">
		    <input class="form-check-input" type="checkbox" value="TRUE" id="provide_transcript">
		    <label class="form-check-label" for="provide_transcript">
			I am able to provide a transcript of my presentation after the fact to aid in accessibility
		    </label>
		</div>

		<hr>

		<h4>Your availability</h4>

		<p>Presentations will be scheduled between <?php echo CONF_START; ?> and <?php echo CONF_END; ?> (inclusive). Please select all the times you would be available to present. You will be contacted by an organizer to confirm your assigned presentation time.</p>

		<?php
		
		// Read the time zone data into memory
		$tzfile = fopen(ABS_PATH . "timezones.csv", "r");
		$timezones = [];
		while (! feof($tzfile)) {
		    $timezones[] = fgetcsv($tzfile);
		}
		fclose($tzfile);

		?>
		<div class="form-group mb-3">
		    <label for="tz_selector">Display times in another time zone</label>
		    <select class="form-control form-control-sm" id="tz_selector">
			<option value="0">UTC</option>
			<?php

			// The variable $tz[2] in the loop below means that we take
			// daylight savings time; switch to $tz[1] for standard time
			foreach ($timezones as $tz) {
			    if ($tz[0] != "timezone" & $tz[0] != "UTC") {
				echo '<option value="' . $tz[2] . '">';
				echo $tz[0];
				echo '</option>';
			    }
			}
			
			?>
		    </select>
		</div>
		<div id="avail_table_change_feedback"></div>

		<div id="availability_table">
		    <?php include (ABS_PATH . "presenters-signup/availability-table.php"); ?>
		</div>
		<input type="hidden" id="checked_slots" value="">
		
		<hr>

		<button id="send_presenter_application" class="btn btn-lg btn-primary" disabled>Sign up to present</button>

	    </div>
	    
	</div>
    </div>
</div>
