<?php

include_once ("../config.php");

$confirm = sch_get_presenter_confirmation_by_link ($_POST['link']);

if ($confirm) {

    $presenter = sch_get_presenter ($confirm['presenter']);

    sch_save_presenter_confirmation_response (
	$confirm['link'],
	$presenter['id'],
	$confirm['slot'],
	$_POST['confirm_time'],
	$_POST['availability'],
	$_POST['name'],
	$_POST['pronouns'],
	$_POST['handle'],
	$_POST['title'],
	$_POST['abstract'],
	$_POST['host'],
	$_POST['hosting_details'],
	$_POST['recording'],
	$_POST['transcript']
    );
    
}

?>
