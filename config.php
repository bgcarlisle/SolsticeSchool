<?php

// MySQL credentials
define('DB_USER', "");
define('DB_PASS', "");
define('DB_NAME', "");
define('DB_HOST', "");

// The absolute file path, include trailing slash
define('ABS_PATH', "");

// The absolute path to PHPMailer, include trailing slash
define('PHP_MAILER_PATH', "");

// The URL to the site, include leading https:// and trailing slash
define('SITE_URL', "");

// The name of the conference
define('CONF_NAME', "");

// The email address that the contact form will send to
define('CONTACT_EMAIL', "");

// The email address that the site will send from
define('SENDER_EMAIL', "");
define('SENDER_USER', "");
define('SENDER_PASS', "");
define('SMTP_HOST', "");
define('SMTP_PORT', "");

// Date range when the presenter signup form will be available
define('PRESENT_SIGNUP_START', "");
define('PRESENT_SIGNUP_END', "");

// Date range when the moderator signup form will be available
define('MOD_SIGNUP_START', "");
define('MOD_SIGNUP_END', "");

// Starting date when the participant signup form will be available
define('PARTIC_SIGNUP_START', "");
// There's no end date because the form disappears after the last
// presentation

// Date range when the conference will occur
define('CONF_START', "");
define('CONF_END', "");

// All date ranges are inclusive

include_once (ABS_PATH . "functions.php");

?>
