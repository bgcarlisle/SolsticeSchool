<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {
    include (ABS_PATH . "moderators-signup/signup.php");
} else {
    include (ABS_PATH . "moderators-signup/no-signup.php");
}


include (ABS_PATH . "footer.php");

?>
