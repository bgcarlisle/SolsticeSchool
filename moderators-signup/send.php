<?php

include_once ("../config.php");

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {

    if ($_POST['policy'] == 1) {

	sch_save_moderator (
	    $_POST['name'],
	    $_POST['pronouns'],
	    $_POST['handle'],
	    $_POST['email'],
	    $_POST['max_mods'],
	    $_POST['host'],
	    $_POST['availability']
	);
	
    }
    
} else {
    echo "<p>This form is not currently accepting submissions.</p>";
}

?>
