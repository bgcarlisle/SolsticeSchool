<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$confirm = sch_get_moderator_confirmation_by_link ($_GET['link']);

$mod = sch_get_moderator($confirm['moderator']);

$presenters = sch_get_presenters ("confirmed-with-matched-mods");

?>

<?php if ($confirm) { ?>
    <!-- Confirmation link found -->
    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		<h3>Confirm moderator details</h3>

		<p>Thank you very much for volunteering to act as a moderator at <?php echo CONF_NAME; ?>! Please confirm that you are available for the following presentations, and we will send an email introducing you and the presenter to each other.</p>

		<div id="moderation-details-container">
		    
		    <hr>

		    <p>I will moderate the following presentations: (check all that you confirm you will moderate)</p>

		    <form>

			<?php
			
			// Read the time zone data into memory
			$tzfile = fopen(ABS_PATH . "timezones.csv", "r");
			$timezones = [];
			while (! feof($tzfile)) {
			    $timezones[] = fgetcsv($tzfile);
			}
			fclose($tzfile);

			?>
			<div class="form-group mb-3">
			    <label for="mod_confirm_tz_selector">Display presentation time(s) in another time zone</label>
			    <select class="form-control form-control-sm" id="mod_confirm_tz_selector">
				<option value="0">UTC</option>
				<?php

				// The variable $tz[2] in the loop below means that we take
				// daylight savings time; switch to $tz[1] for standard time
				foreach ($timezones as $tz) {
				    if ($tz[0] != "timezone" & $tz[0] != "UTC") {
					echo '<option value="' . $tz[2] . '">';
					echo $tz[0];
					echo '</option>';
				    }
				}
				
				?>
			    </select>
			</div>

			<div class="table-responsive">
			    <table class="table table-striped">
				<thead>
				    <tr>
					<td scope="col">
					    &nbsp;
					</td>
					<td scope="col">
					    Details
					</td>
					<td scope="col" style="text-align: right;">
					    Time
					</td>
				    </tr>
				</thead>
				<tbody>
				    <?php foreach ($presenters as $pre) { if ($pre['moderators_id'] == $mod['id']) { ?>
					<tr>
					    <td>
						<div class="form-check mx-3">
						    <input id="mod-confirm-presenter-<?php echo $pre['id']; ?>" class="form-check-input mod-confirm-time-checkbox" type="checkbox" value="TRUE" data-presenter="<?php echo $pre['id']; ?>">
						</div>
					    </td>
					    <td>
						<label for="mod-confirm-presenter-<?php echo $pre['id']; ?>">
						    <h5><?php echo $pre['title']; ?></h5>
						</label>
						<h6 class="text-muted"><?php echo $pre['name']; ?><?php if ($pre['pronouns'] != "") { echo " (" . $pre['pronouns'] . ")"; } ?></h6>
						<h6 class="text-muted mb-3"><?php echo $pre['handle']; ?></h6>
						<?php if ($pre['able_to_host'] == 0) { ?>
						    <div id="mod-link-input-container-<?php echo $pre['id']; ?>" style="display: none;">
							<div class="alert alert-warning" role="alert">
							    The presenter has not requested to provide their own video conferencing room for this talk, and so you will be provided with a link and sign-in to a video conferencing room provided by <?php echo CONF_NAME; ?>.
							</div>
						    </div>
						<?php } ?>
					    </td>
					    <td>
						<?php

						$utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($pre['confirmed_slot'] - 1) * 60 * 60) . " UTC";
						
						?>
						<p><span class="signup_time" data-slot="<?php echo $pre['confirmed_slot']; ?>"><?php echo $utc_time; ?></span></p>
						<p><a href="<?php echo SITE_URL; ?>programme/calendar.php?presenter=<?php echo $pre['id']; ?>">Download event as an .ics file</a></p>
					    </td>
					</tr>
				    <?php } } ?>
				</tbody>
			    </table>
			</div>
			<p id="mod-multiple-calendar-dl" style="display: none;">
			    <a href="<?php echo SITE_URL; ?>programme/calendar-all.php">Download checked presentations as an .ics file</a>
			</p>

			<input type="hidden" id="checked_presentations" name="checked_presentations" value="">

			<div class="form-check mb-3">
			    <input id="mod-confirm-policy" class="form-check-input" type="checkbox" value="TRUE">
			    <label for="mod-confirm-policy">I have read, understood and agree to the <a href="<?php echo SITE_URL; ?>moderators/">moderation policies</a> for <?php echo CONF_NAME; ?></label>
			</div>

			<p>We will introduce you and the presenter to each other by email. We recommend that you briefly speak with each other to discuss the pronunciation of names, pronouns, how the presenter wishes to be introduced and whether any other accommodations need to be made.</p>

			<input type="hidden" name="link" value="<?php echo $_GET['link']; ?>">

		    </form>
		    
		    <button id="save-mod-confirm-button" class="btn btn-lg btn-primary" disabled>Confirm moderation details</button>
		    
		</div>
		
	    </div>
	</div>
    </div>    
<?php } else { ?>
    <!-- Confirmation link not found -->
    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		<div class="alert alert-danger" role="alert">
		    Error opening confirmation link: invalid, expired, or database connection issue
		</div>
		
	    </div>
	</div>
    </div>
<?php } ?>

<?php

include (ABS_PATH . "footer.php");

?>
