<?php

include_once ("../config.php");

$confirm = sch_get_moderator_confirmation_by_link ($_POST['link']);

if ($confirm) {
    sch_save_moderator_confirmation_response ($confirm, $_POST['checked_presentations']);
}

?>
