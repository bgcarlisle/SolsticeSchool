<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>About <?php echo CONF_NAME; ?></h3>

	    <p>Scholar Social is hosting <?php echo CONF_NAME; ?>, an informal online conference covering a variety of topics, with presentations occurring between <?php echo CONF_START; ?> and <?php echo CONF_END; ?>. Our goals are to:</p>

	    <ul>
		<li>provide scholars with a low-stakes, non-pretentious venue for talking about our work and receiving feedback</li>
		<li>provide all of the Fediverse knowledge and insight from people of diverse backgrounds, and</li>
		<li>carry forward the ethics of Scholar Social regarding freedom of knowledge, accessibility and mutual support.</li>
	    </ul>

	    <p>Information about previous years including conference programmes and recordings of presentations can be found at <a href="https://solsticeschool.scholar.social/">summerschool.scholar.social</a>.</p>

	    <p>The volunteer organizing team for <?php echo CONF_NAME; ?> consists of the following members:</p>

	    <ul>
		<li>Cynthia Li (@cxli@scholar.social)</li>
		<li>Kat (@KatLH@scholar.social and @koosli@aus.social)</li>
		<li>The research fairy (@bgcarlisle@scholar.social)</li>
		<li>Danny Chan (@danwchan@scholar.social)</li>
		<li>Riley (@Cyborgneticz@scholar.social)</li>
	    </ul>

	    <p>The <a href="<?php echo SITE_URL; ?>presenters-signup/">signup form for presenters</a> is open from <?php echo PRESENT_SIGNUP_START; ?> to <?php echo PRESENT_SIGNUP_END; ?>.</p>
	    
	    <p>The <a href="<?php echo SITE_URL; ?>moderators-signup/">signup form for moderators</a> is open from <?php echo MOD_SIGNUP_START; ?> to <?php echo MOD_SIGNUP_END; ?>.</p>
	    
	    <p>The <a href="<?php echo SITE_URL; ?>programme/">programme and signup form for participants</a> is open starting <?php echo PARTIC_SIGNUP_START; ?> and you can sign up for any presentation until the presentation ends; after the conference, the programme will remain visible for archival purposes.</p>
	    
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
