<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <?php $deleted = sch_delete_presenter_permanently ($_GET['link']); ?>
	    <?php if ($deleted) { ?>
		<h3>Your entry has been deleted</h3>
		<p>Your entry has been successfully deleted. Thank you for presenting at <?php echo CONF_NAME; ?>.</p>
	    <?php } else { ?>
		<div class="alert alert-danger" role="alert">
		    Error deleting entry
		</div>
	    <?php } ?>
	</div>
    </div>
</div>
