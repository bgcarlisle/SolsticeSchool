<?php

include_once("../config.php");

$link = sch_get_archive_link ($_POST['link']);

if ($link) {
    
    if ($_FILES['file']['error'] == 0) { // There's no error


	$presenter_dir = substr(md5($link['presenter']), 0, 12);

	if ( ! is_dir (ABS_PATH . "archive/files/") ) {
	    mkdir(ABS_PATH . "archive/files/", 0777);
	} else {
	    chmod(ABS_PATH . "archive/files/", 0777);
	}

	if ( ! is_dir (ABS_PATH . "archive/files/" . $presenter_dir . "/") ) {
	    mkdir(ABS_PATH . "archive/files/" . $presenter_dir . "/", 0777);
	} else {
	    chmod(ABS_PATH . "archive/files" . $presenter_dir . "/", 0777);
	}

	if ( ! file_exists (ABS_PATH . "archive/files/" . $presenter_dir . "/" . $_FILES['file-to-upload']['name']) ) { // A file with the same name doesn't already exist
	    if (move_uploaded_file ($_FILES['file-to-upload']['tmp_name'], ABS_PATH . "archive/files/" . $presenter_dir . "/" . $_FILES['file-to-upload']['name'])) {
		// Moving the uploaded file succeeded
		$response[] = 1;
		$response[] = $_FILES['file-to-upload']['name'];
		echo json_encode($response);
	    } else { // Moving the uploaded file failed
		$response[] = 0;
		$response[] = "Error: File upload not successful. Try again!";
		echo json_encode($response);
	    }
	} else { // A file with the same name already exists
	    $response[] = 0;
	    $response[] = "Error: File already exists. Rename the local file before upload or remove the file on the server.";
	    echo json_encode($response);
	}
	
	
    } else { // There's an error
	$response[] = 0;
	$response[] = "Error: File upload not successful. Try again!";
	echo json_encode($response);
    }
    
}

?>
