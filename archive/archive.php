<div id="action-mask"></div>
<div class="action-prompt" id="upload-file-prompt">
    <button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
    <h4><img src="<?php echo SITE_URL ?>images/file-earmark-arrow-down.svg"> Add a file</h4>
    <form id="file-upload-form" action="<?php echo SITE_URL; ?>archive/upload-file.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="link" value="<?php echo $_GET['link']; ?>">
	<div class="form-group mb-3">
	    <label for="file-upload-selector" class="form-label">Select a file to add to your presentation archive (max 200 MB)</label>
	    <p><small class="text-muted">If you need to upload a file greater than 200 MB, <a href="<?php echo SITE_URL; ?>contact/">contact us</a> and we will make arrangements.</small></p>
	    <input type="file" class="form-control" id="file-upload-selector" name="file-to-upload">
	</div>
	<button class="btn btn-primary" id="add-file-to-materials" disabled>Upload file</button>
	<button class="btn btn-secondary close-prompt">Cancel</button>
    </form>
    <div id="file-upload-progress" class="mt-3" style="display: none;">Upload in progress; please wait ...</div>
    <div class="alert alert-danger mt-3" role="alert" style="display: none;" id="file-upload-feedback">
    </div>
    <p class="mt-3"><small class="text-muted">All uploaded files will be made freely available to anyone who visits the archived conference programme page, so be sure to only upload files that you want publicly available and that won't cause us any trouble (no copyrighted stuff, no files with malware). As much as is possible given the nature of publishing conference materials, it is the policy of <?php echo CONF_NAME; ?> that presenters retain complete creative and legal control of their own materials.</small></p>
</div>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
		<?php if ($_POST['action'] == "save") { ?>

		    <?php $archive_saved = sch_save_changes_to_archive ($_GET['link'], $_POST['hidden_from_archive_presenter'], $_POST['name'], $_POST['pronouns'], $_POST['handle'], $_POST['email'], $_POST['title'], $_POST['abstract']); ?>

		    <?php if ($archive_saved) { ?>
			<?php $presenter = sch_get_presenter ($link['presenter']); ?>
			<div class="alert alert-success" role="alert">
			    Changes saved
			</div>
		    <?php } else { ?>
			<div class="alert alert-danger" role="alert">
			    Error saving changes
			</div>
		    <?php } ?>
		    
		<?php } ?>
	    </div>
	    
	    <h3>Presentation materials archive</h3>

	    <h5 class="text-muted">"<?php echo $presenter['title']; ?>" by <?php echo $presenter['name']; ?></h5>

	    <form action="<?php echo SITE_URL; ?>archive/<?php echo $_GET['link']; ?>" method="post">
		
		<input type="hidden" name="action" value="save">

		<p>You can edit your contact details and upload presentation materials that will be listed on your entry on the archival version of the conference programme. You can also use this page to hide your entry temporarily or even delete it completely at any time. If this link expires, you can request a new link from the archived programe page.</p>

		<div class="form-group">
		    <label for="visibility-selector">Visibility of your entry on the conference archive</label>
		    <select class="form-control" id="visibility-selector" name="hidden_from_archive_presenter">
			<option value="0"<?php if ($presenter['hidden_from_archive_presenter'] == 0) { echo " selected"; } ?>>Visible on archive</option>
			<option value="1"<?php if ($presenter['hidden_from_archive_presenter'] == 1) { echo " selected"; } ?>>Hidden from archive</option>
		    </select>
		    <small class="form-text text-muted">Hide or make your presentation visible on the archival version of the conference programme</small>
		</div>

		<hr>

		<h4>About you</h4>

		<div class="form-group">
		    <label for="presenter_name">Your name</label>
		    <input type="text" class="form-control" id="presenter_name" name="name" placeholder="E.g. Otter" value="<?php echo str_replace("\"", "&quot;", $presenter['name']); ?>">
		    <small id="presenter_name_help" class="form-text text-muted">
			This doesn't need to be your legal name; use the name that you would like us to use to refer to you.
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="presenter_pronouns">Your pronouns</label>
		    <input type="text" class="form-control" id="presenter_pronouns" name="pronouns" placeholder="E.g. they/them" value="<?php echo str_replace("\"", "&quot;", $presenter['pronouns']); ?>">
		    <small id="presenter_pronouns_help" class="form-text text-muted">
			Optional
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="presenter_handle">Your handle on the Fediverse</label>
		    <input type="text" class="form-control" id="presenter_handle" name="handle" placeholder="@SummerSchool@scholar.social" value="<?php echo str_replace("\"", "&quot;", $presenter['handle']); ?>">
		    <small id="presenter_handle_help" class="form-text text-muted">
			Use the handle that you would like to be credited for your presentation
		    </small>
		</div>
		
		<div class="form-group mb-3">
		    <label for="presenter_email">Your email</label>
		    <input type="email" class="form-control" id="presenter_email" name="email" placeholder="yourname@domain.muffin" value="<?php echo str_replace("\"", "&quot;", $presenter['email']); ?>">
		    <small id="presenter_email_help" class="form-text text-muted">
			Use an email address where you can be reached to confirm details of your presentation, like finalizing the date/time, matching you with a moderator, etc.
		    </small>
		</div>

		<hr>
		
		<h4>Upload presentation materials</h4>

		<p>Please use the form below to add links, text or attach files to the archival version of the conference programme. If uploaded here, they will be made available freely to anyone who visits the conference site, so be sure to only upload files that you want publicly available and that won't cause us any trouble (no copyrighted stuff, no files with malware, etc.). You can always edit or remove these materials later.</p>
		
		<hr>

		<?php $materials = sch_get_materials_for_presenter($presenter['id']); ?>

		<div id="materials-container">
		    <?php foreach ($materials as $mat) { ?>

			<?php if ($mat['materials_type'] == "link") { ?>
			    <div class="card mb-3">
				<div class="card-body">
				    <a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
				    <button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
				    <button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
				    
				    <h5 class="card-title"><img src="<?php echo SITE_URL ?>images/link-45deg.svg"> Link</h5>
				    <p>The following link will be inserted into your entry on the archived programme for the conference</p>

				    <input type="hidden" name="mattypes[]" value="link">
		
				    <div class="form-group">
					<label for="materials_title_<?php echo $mat['id']; ?>">Link text</label>
					<input type="text" class="form-control" id="materials_title_<?php echo $mat['id']; ?>" name="mattitles[]" placeholder="E.g. A blog post of mine where I expand on this topic" value="<?php echo str_replace("\"", "&quot;", $mat['materials_title']); ?>">
					<small class="form-text text-muted">
					    If left blank, the text will just be the word "Link"
					</small>
				    </div>
		
				    <div class="form-group mb-3">
					<label for="materials_content_<?php echo $mat['id']; ?>">Link URL</label>
					<input type="text" class="form-control" id="materials_content_<?php echo $mat['id']; ?>" name="matcontent[]" placeholder="E.g. <?php echo SITE_URL; ?>programme/" value="<?php echo str_replace("\"", "&quot;", $mat['materials_content']); ?>">
					<small class="form-text text-muted">
					    Make sure to begin link with "http://" or "https://"; if left blank, this link will be removed
					</small>
				    </div>

				    <button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
				    <button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
				    
				</div>
			    </div>
			<?php } ?>

			<?php if ($mat['materials_type'] == "text") { ?>
			    <div class="card mb-3">
				<div class="card-body">
				    <a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
				    <button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
				    <button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
				    
				    <h5 class="card-title"><img src="<?php echo SITE_URL ?>images/journal-text.svg"> Text</h5>
				    <p>The following text will be inserted into your entry on the archived programme for the conference</p>

				    <input type="hidden" name="mattypes[]" value="text">
				    
				    <div class="form-group">
					<label for="materials_title_<?php echo $mat['id']; ?>">Title</label>
					<input type="text" class="form-control" id="materials_title_<?php echo $mat['id']; ?>" name="mattitles[]" placeholder="E.g. A transcript of my presentation" value="<?php echo str_replace("\"", "&quot;", $mat['materials_title']); ?>">
					<small class="form-text text-muted">
					    If left blank, the text will be given the title "Text"
					</small>
				    </div>

				    <div class="form-group mb-3">
					<label for="materials_content_<?php echo $mat['id']; ?>"><img src="<?php echo SITE_URL ?>images/journal-text.svg"> Text</label>
					<textarea class="form-control" id="materials_content_<?php echo $mat['id']; ?>" rows="8" name="matcontent[]"><?php echo $mat['materials_content']; ?></textarea>
					<small class="form-text text-muted">
					    Line breaks will be displayed, but no other markup will be respected; if left blank, this text entry will be removed
					</small>
					
				    </div>

				    <button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
				    <button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
				    
				</div>
			    </div>
			<?php } ?>

			<?php if ($mat['materials_type'] == "file") { ?>
			    <?php $presenter_dir = substr(md5($link['presenter']), 0, 12); ?>
			    <div class="card mb-3">
				<div class="card-body">
				    <a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
				    <button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
				    <button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
				    
				    <h5 class="card-title"><img src="<?php echo SITE_URL ?>images/file-earmark-arrow-down.svg"> File</h5>
				    <p>A link to the following file will be inserted into your entry on the archived programme for the conference</p>

				    <input type="hidden" name="mattypes[]" value="file">
				    
				    <div class="form-group">
					<label for="materials_title_<?php echo $mat['id']; ?>">Title</label>
					<input type="text" class="form-control" id="materials_title_<?php echo $mat['id']; ?>" name="mattitles[]" placeholder="E.g. A video recording of the presentation" value="<?php echo str_replace("\"", "&quot;", $mat['materials_title']); ?>">
					<small class="form-text text-muted">
					    If left blank, the file will be labelled "File"
					</small>
				    </div>

				    <div class="mt-3 mb-3">
					<a href="<?php echo SITE_URL; ?>archive/files/<?php echo $presenter_dir; ?>/<?php echo $mat['materials_content']; ?>"><?php echo $mat['materials_content']; ?></a>
				    </div>

				    <input type="hidden" name="matcontent[]" value="<?php echo $mat['materials_content']; ?>">

				    <button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
				    <button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
				    
				</div>
			    </div>
			<?php } ?>
			
		    <?php } ?>
		</div>

		<button class="btn btn-sm btn-secondary" id="add-link-to-materials">Add link</button>
		<button class="btn btn-sm btn-secondary" id="add-text-to-materials">Add text</button>
		<button class="btn btn-sm btn-secondary" id="add-file-to-materials-prompt">Add file</button>

		<hr>

		<button class="btn btn-lg btn-primary">Save changes</button>

	    </form>

	    
	    <div style="display: none;"><!-- These are just here to be copied by the js -->

		<!-- Start of link template -->
		<div id="link-materials-template">
		    <div class="card-body">
			<a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
			<button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
			<button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
			
			<h5 class="card-title"><img src="<?php echo SITE_URL ?>images/link-45deg.svg"> Link</h5>
			<p>The following link will be inserted into your entry on the archived programme for the conference</p>

			<input type="hidden" name="mattypes[]" value="link">
			
			<div class="form-group">
			    <label for="materials_title_[matid]">Link text</label>
			    <input type="text" class="form-control" id="materials_title_[matid]" name="mattitles[]" placeholder="E.g. A blog post of mine where I expand on this topic" value="">
			    <small class="form-text text-muted">
				If left blank, the text will just be the word "Link"
			    </small>
			</div>
			
			<div class="form-group mb-3">
			    <label for="materials_content_[matid]">Link URL</label>
			    <input type="text" class="form-control" id="materials_content_[matid]" name="matcontent[]" placeholder="E.g. <?php echo SITE_URL; ?>programme/" value="">
			    <small class="form-text text-muted">
				Make sure to begin link with "http://" or "https://"; if left blank, this link will be removed
			    </small>
			</div>

			<button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
			<button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
			
		    </div>
		</div>
		<!-- End of link template -->

		<!-- Start of text template -->
		<div id="text-materials-template">
		    <div class="card-body">
			<a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
			<button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
			<button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
			
			<h5 class="card-title"><img src="<?php echo SITE_URL ?>images/journal-text.svg"> Text</h5>
			<p>The following text will be inserted into your entry on the archived programme for the conference</p>

			<input type="hidden" name="mattypes[]" value="text">
			
			<div class="form-group">
			    <label for="materials_title_[matid]">Title</label>
			    <input type="text" class="form-control" id="materials_title_[matid]" name="mattitles[]" placeholder="E.g. A transcript of my presentation" value="">
			    <small class="form-text text-muted">
				If left blank, the text will be given the title "Text"
			    </small>
			</div>

			<div class="form-group mb-3">
			    <label for="materials_content_[matid]">Text</label>
			    <textarea class="form-control" id="materials_content_[matid]" rows="8" name="matcontent[]"></textarea>
			    <small class="form-text text-muted">
				Line breaks will be displayed, but no other markup will be respected; if left blank, this text entry will be removed
			    </small>
			    
			</div>

			<button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
			<button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
			
		    </div>
		</div>
		<!-- End of text template -->

		<!-- Start of file template -->
		<div id="file-materials-template">
		    <div class="card-body">
			<a href="#" style="float: right;" onclick="event.preventDefault();$(this).slideUp(0);$(this).parent().children('.removal-buttons').slideDown();"><small>Remove</small></a>
			<button class="btn btn-sm btn-secondary removal-buttons" style="float: right; display: none;" onclick="event.preventDefault();$(this).parent().children('.removal-buttons').slideUp(0);$(this).prev().slideDown();">Cancel</button>
			<button class="btn btn-sm btn-danger mat-remove removal-buttons" style="float: right; display: none; margin-right: 5px;">Remove</button>
			
			<h5 class="card-title"><img src="<?php echo SITE_URL ?>images/file-earmark-arrow-down.svg"> File</h5>
			<p>A link to the following file will be inserted into your entry on the archived programme for the conference</p>

			<input type="hidden" name="mattypes[]" value="file">
			
			<div class="form-group">
			    <label for="materials_title_<?php echo $mat['id']; ?>">Title</label>
			    <input type="text" class="form-control" id="materials_title_<?php echo $mat['id']; ?>" name="mattitles[]" placeholder="E.g. A video recording of the presentation" value="">
			    <small class="form-text text-muted">
				If left blank, the file will be labelled "File"
			    </small>
			</div>

			<div class="mt-3 mb-3">
			    <a href="<?php echo SITE_URL; ?>archive/files/<?php echo $presenter_dir; ?>/[matcontent]">[matcontent]</a>
			</div>

			<input type="hidden" name="matcontent[]" value="[matcontent]">

			<button class="btn btn-sm btn-secondary mat-move-up">Move up</button>
			<button class="btn btn-sm btn-secondary mat-move-down">Move down</button>
			
		    </div>
		</div>
		<!-- End of file template -->
		
	    </div>

	    <div class="mb-3 mt-3" id="delete-link"><small><a href="#" onclick="event.preventDefault();$('#delete-presentation-form').slideDown();$('#delete-link').slideUp();">Delete your presentation's entry</a></small></div>

	    <form action="<?php echo SITE_URL; ?>archive/<?php echo $_GET['link']; ?>" method="post" style="display: none;" id="delete-presentation-form">
		<input type="hidden" name="action" value="delete">
		<hr>
		
		<h4>Delete your entry permanently</h4>
		
		<div class="alert alert-danger" role="alert">
		    Warning: this will irreversibly delete your presentation and there will be no way to restore it
		</div>

		<button class="btn btn-lg btn-danger">Delete</button>
		<a href="#" class="btn btn-lg btn-secondary" onclick="event.preventDefault();$('#delete-presentation-form').slideUp();$('#delete-link').slideDown();">Cancel</a>
	    </form>
	    
	</div>
    </div>
</div>
