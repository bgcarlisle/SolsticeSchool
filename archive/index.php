<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$link = sch_get_archive_link ($_GET['link']);

if ($link) {
    $presenter = sch_get_presenter ($link['presenter']);
    if ($_POST['action'] != "delete") {
	include (ABS_PATH . "archive/archive.php");
    } else {
	include (ABS_PATH . "archive/delete.php");
    }
} else {
    include (ABS_PATH . "archive/no-archive.php");
}

include (ABS_PATH . "footer.php")

?>
