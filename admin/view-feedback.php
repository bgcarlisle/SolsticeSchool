<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$feedback = sch_get_feedback ();

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">View feedback</li>
		</ol>
	    </nav>
	    <h3>View feedback</h3>

	    <div class="table-responsive mb-3">
		<table class="table table-striped table-sm">
		    <thead>
			<td scope="col">
			    Feedback details
			</td>
			<td scope="col">
			    Comment
			</td>
		    </thead>
		    <tbody>
			<?php foreach ($feedback as $fb) { ?>
			    <tr>
				<td>
				    <p>Feedback on presentation: '<?php echo $fb['title']; ?>' by <?php echo $fb['name']; ?></p>
				    <?php if ($fb['contact'] != "") { ?>
					<p>Comment from: <?php echo $fb['contact']; ?></p>
				    <?php } ?>
				    <p>Sent: <?php echo $fb['utc_when_sent']; ?> UTC</p>
				</td>
				<td><?php echo $fb['comments']; ?></td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
	    
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>

