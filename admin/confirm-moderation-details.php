<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$mods = sch_get_moderators ("matched");
$presenters = sch_get_presenters ("confirmed-with-matched-mods");

$confirmation_deadline = date("Y-m-d (D) H:i", strtotime(MOD_SIGNUP_END) + 86400) . " UTC";

?>
<div id="action-mask"></div>
<?php foreach ($mods as $mod) { ?>
    <div class="action-prompt" id="send-mod-confirm-<?php echo $mod['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<div id="send-mod-confirm-inner-<?php echo $mod['id']; ?>">
	    <h4>Send confirmation email to <?php echo $mod['name']; ?></h4>
	    <hr>
	    <p><strong>To:</strong> <?php echo $mod['name']; ?> &lt;<?php echo $mod['email']; ?>&gt;</p>
	    <p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>
	    <p><strong>Subject:</strong> Thank you for volunteering to moderate at <?php echo CONF_NAME; ?>; please confirm your availability</p>
	    <hr>
	    <p>Dear <?php echo $mod['name']; ?>,</p>
	    <p>Thank you very much for volunteering to act as a moderator at <?php echo CONF_NAME; ?>.</p>
	    <p>We have matched you with the following presentation(s) and request that you confirm that you are available to moderate these talks:</p>

	    <ul>
		<?php foreach ($presenters as $pre) { if ($pre['moderators_id'] == $mod['id']) { ?>
		    <?php $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC"; ?>
		    <li>'<?php echo $pre['title']; ?>' by <?php echo $pre['name']; ?> <?php if ($pre['pronouns'] != "") echo "(" . $pre['pronouns'] . ")"; ?> at <?php echo $presentation_time_utc; ?></li>
		<?php } } ?>
	    </ul>

	    <p>Please confirm your availability using the following link as soon as possible, as we wish to have all moderator-presenter pairs confirmed by <?php echo $confirmation_deadline; ?>.</p>
	    <p>[Link will be inserted here]</p>
	    <p>When you have confirmed your availability, we will email both you and the presenter to make introductions, and we recommend that you briefly speak with each other to discuss the pronunciation of names, pronouns, how the presenter wishes to be introduced and whether any other accommodations need to be made.</p>
	    <p>Best regards,</p>
	    <p>The organization team<br><?php echo CONF_NAME; ?></p>
	    <hr>
	    <div class="alert alert-danger" role="alert">Warning: Sending an email cannot be undone</div>
	    <form action="<?php echo SITE_URL; ?>admin/confirm-moderation-details.php" method="post">
		<input type="hidden" name="action" value="send">
		<input type="hidden" name="mid" value="<?php echo $mod['id']; ?>">
		<button class="btn btn-lg btn-success send-mod-confirm" data-mod="<?php echo $mod['id']; ?>">Send email</button>
		<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	    </form>
	</div>
    </div>
<?php } ?>
<?php foreach ($mods as $mod) { ?>
    <div class="action-prompt" id="clear-mod-confirm-<?php echo $mod['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<div id="clear-mod-confirm-inner-<?php echo $mod['id']; ?>">
	    <h4>Clear <?php echo $mod['name']; ?></h4>
	    <p>Clicking "clear" below will deactivate any links that have been sent.</p>
	    <p>If the moderator has already confirmed that they will moderate, this will also un-confirm them for all presentations.</p>
	    <div class="alert alert-danger" role="alert">Warning: This cannot be undone</div>
	    <form action="<?php echo SITE_URL; ?>admin/confirm-moderation-details.php" method="post">
		<input type="hidden" name="action" value="clear">
		<input type="hidden" name="mid" value="<?php echo $mod['id']; ?>">
		<button class="btn btn-lg btn-danger clear-mod-confirm" data-mod="<?php echo $mod['id']; ?>">Clear links and confirmation</button>
		<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	    </form>
	</div>
    </div>
<?php } ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
		<?php

		if ($_POST['action'] == "send") {
		    sch_send_mod_confirmation_email ($_POST['mid']);
		}

		if ($_POST['action'] == "clear") {
		    sch_clear_mod_confirmation ($_POST['mid']);
		    $presenters = sch_get_presenters ("confirmed-with-matched-mods");
		}
		
		?>
	    </div>
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Confirm moderation details</li>
		</ol>
	    </nav>
	    <h3>Confirm moderation details</h3>

	    <p>Each row of the table below contains one moderator that has been matched with at least one presenter on the "Match moderators to presenters" page. Because a moderator may be matched to many presenters, there may be many entries under the "Status" column for a moderator.</p>

	    <p>Clicking "Send" prompts you to send an email to the moderator in question, which will invite them to confirm that they will moderate the presentations they've been matched to. They will have the option to confirm all, some or none of the matches that they are sent. This will be reflected in the "Status" column. Once the link has been used and the moderator has clicked "Save" on the page provided, the entry under "Emailed links" for that moderator will change to "Link closed." So if the link is closed and there are presenters named under "Status" that are still "Not confirmed," that means those matches have been declined, and must be re-assigned using the "Match moderators to presenters" page.</p>

	    <p>Clicking "Clear" prompts you to clear both the links that have been sent and the confirmations that have been made by the moderator.</p>

	    <div class="table-responsive mb-3">
		<table class="table table-striped table-bordered table-sm">
		    <thead>
			<tr>
			    <td scope="col">
				Moderator
			    </td>
			    <td scope="col">
				Emailed links
			    </td>
			    <td scope="col">
				Status
			    </td>
			    <td scope="col" style="text-align: right;">
				Actions
			    </td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($mods as $mod) { ?>
			    <?php $show_clear_button = FALSE; ?>
			    <tr>
				<td scope="row">
				    <p>
					<?php echo $mod['name']; ?><?php if ($mod['pronouns'] != "") echo " (" . $mod['pronouns'] . ")"; ?>
					<br>
					<span class="text-muted"><?php echo $mod['email']; ?></span>
					<br>
					<span class="text-muted"><?php echo $mod['handle']; ?></span>
				    </p>
				</td>
				<td>
				    <?php $links = sch_get_all_links_for_moderator ($mod['id']); ?>
				    <?php foreach ($links as $link) { ?>
					<?php if ($link['active'] == 1) { ?>
					    <span class="badge bg-success" title="The link that was sent to the moderator can still be used to confirm their details">Link is active</span>
					    <?php $show_clear_button = TRUE; ?>
					<?php } else { ?>
					    <span class="badge bg-secondary" title="The link that was sent to the moderator has either been used or it has been cleared by an admin">Link closed</span>
					<?php } ?>
				    <?php } ?>
				</td>
				<td>
				    <?php foreach ($presenters as $pre) { if ($pre['moderators_id'] == $mod['id']) { ?>
					<?php if ($pre['confirmed_mod'] == $mod['id']) { ?>
					    <span class="badge bg-success" title="The moderator has confirmed that they will moderate the presentation for <?php echo $pre['name']; ?>">Confirmed (<?php echo $pre['name'] ?>)</span>
					    <?php $show_clear_button = TRUE; ?>
					<?php } else { ?>
					    <span class="badge bg-secondary" title="The moderator has not confirmed that they will moderate the presentation for <?php echo $pre['name']; ?>">Not confirmed (<?php echo $pre['name'] ?>)</span>
					<?php } ?>
				    <?php } } ?>
				</td>
				<td style="text-align: right;">
				    <button class="btn mb-2 btn-sm btn-primary prompt-send-mod-confirm" data-mod="<?php echo $mod['id']; ?>">Send</button>
				    <?php if ($show_clear_button) { ?>
					<button id="prompt-clear-mod-link-<?php echo $mod['id']; ?>" class="btn mb-2 btn-sm btn-secondary prompt-clear-mod-link" data-mod="<?php echo $mod['id']; ?>">Clear</button>
				    <?php } ?>
				</td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
	    
	</div>
    </div>
</div>

<?php include_once (ABS_PATH . "footer.php"); ?>
