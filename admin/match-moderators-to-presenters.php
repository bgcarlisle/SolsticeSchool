<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("confirmed");

$moderators = sch_get_moderators ("accepted");

$avail = sch_get_moderator_availabilities ();

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Match moderators to presenters</li>
		</ol>
	    </nav>
	    <h3>Match moderators to presenters</h3>

	    <p>The grid below is populated by one moderator per column and one confirmed presenter per row. To remove a moderator from this grid, go to the "Review moderators" and save their moderator status as "Rejected."</p>
	    <p>Checkboxes are only provided where the moderator has indicated that they are available for the presentation in question and where the moderator and presenter do not have the same email address. When a presenter is matched to a moderator, the "Status" column changes to "Matched" and the other checkboxes in that row are disabled to prevent booking two moderators for a presentation. The bottom row is a fraction for each moderator where the numerator is the number of presentations they have been matched with and the denominator is the number of presentations they indicated that they are willing to moderate.</p>
	    <p>Changes are not saved until you click "Save changes" at the bottom. The "Confirm moderation details" page is populated with moderators who have been matched to presenters.</p>

	    <div class="table-responsive mb-3">
		<table class="table table-striped table-bordered table-sm">
		    <thead>
			<tr>
			    <th scope="col" colspan="2">Presenter</th>
			    <?php foreach ($moderators as $mod) { ?>
				<th scope="col" style="text-align: right;">
				    <?php echo $mod['name']; ?>
				</th>
			    <?php } ?>
			</tr>
		    </thead>
		    <tbody>
			<?php $modmatchcount = []; ?>
			<?php foreach ($presenters as $pre) { ?>
			    <tr>
				<th scope="row"><?php echo $pre['name']; ?></th>
				<?php if (is_null($pre['matched_mod'])) { ?>
				    <td class="table-warning presenter_match_status_<?php echo $pre['id']; ?>">Waiting</td>
				<?php } else { ?>
				    <td class="table-success presenter_match_status_<?php echo $pre['id']; ?>">Matched</td>
				<?php } ?>
				<?php foreach ($moderators as $mod) { ?>
				    <?php $mod_avail = sch_mod_is_avail($avail, $mod['id'], $pre['id'], $pre['email']); ?>
				    <?php if ($mod_avail) { ?>
					<td style="text-align: right;">
					    <input class="form-check-input match-mods-check match-mods-check-mod-<?php echo $mod['id']; ?> match-mods-check-pre-<?php echo $pre['id']; ?>" data-presenter="<?php echo $pre['id']; ?>" data-moderator="<?php echo $mod['id']; ?>" type="checkbox"<?php if ($pre['matched_mod'] == $mod['id']) { echo " checked"; $modmatchcount[$mod['id']]++; } ?><?php if (! is_null($pre['matched_mod']) & $pre['matched_mod'] != $mod['id']) { echo " disabled"; } ?>>
					</td>
				    <?php } else { ?>
					<td></td>
				    <?php } ?>
				<?php } ?>
			    </tr>
			<?php } ?>
			<tr>
			    <th scope="row" colspan="2">Matched</th>
			    <?php foreach ($moderators as $mod) { ?>
				<?php

				if (intval($modmatchcount[$mod['id']]) <= $mod['max_mods']) {
				    $matchcountclass = "table-success";
				}
				
				if (intval($modmatchcount[$mod['id']]) > $mod['max_mods']) {
				    $matchcountclass = "table-danger";
				}
				
				if (intval($modmatchcount[$mod['id']]) == 0) {
				    $matchcountclass = "";
				}
				
				?>
				<td id="mod-number-matched-cell-<?php echo $mod['id']; ?>" class="<?php echo $matchcountclass; ?>" style="text-align: right;">
				    <span id="mod-number-matched-<?php echo $mod['id']; ?>" data-maxmods="<?php echo $mod['max_mods']; ?>"><?php echo intval($modmatchcount[$mod['id']]); ?></span>/<?php echo $mod['max_mods']; ?>
				</td>
			    <?php } ?>
			</tr>
		    </tbody>
		</table>
	    </div>
	    <p><a href="<?php echo SITE_URL; ?>admin/final-detailed-schedule.php?confirmed=no">Download tentative detailed schedule as a .csv file</a></p>
	    <input type="hidden" id="matched-mod-selections">
	    <button id="save-matched-mods-button" class="btn btn-primary btn-lg" disabled>Save changes</button>
	    <div id="matched_mods_save_feedback" class="alert mt-3" role="alert" style="display: none;"></div>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
