<?php

include_once ("../config.php");

$presenters = sch_get_presenters ("confirmed");

$filename = preg_replace("/[^a-zA-Z0-9]/", "_", CONF_NAME) . ".csv";
$filename = preg_replace("/[_]+/", "_", $filename);

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename);

?>date,time,title,abstract,presenter_name,presenter_pronouns,presenter_handle
<?php

foreach ($presenters as $presenter) {

    $utc_date = date("Y-m-d", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60);
    $utc_time = date("H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60);
    
    echo $utc_date . ",";
    echo $utc_time . ",";
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['title']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['abstract']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['name']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['pronouns']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['handle']) . '"' . "\n";
  
}

?>
