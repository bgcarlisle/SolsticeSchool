<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("confirmed-with-confirmed-mods");

?>
<div id="action-mask"></div>
<?php foreach ($presenters as $pre) { ?>
    <?php
    
    $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";
    
    ?>
    <div class="action-prompt" id="send-pre-intro-<?php echo $pre['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<h4>Send introduction email to <?php echo $pre['name']; ?></h4>
	<hr>
	<p><strong>To:</strong> <?php echo $pre['name']; ?> &lt;<?php echo $pre['email']; ?>&gt;</p>
	<p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>
	<p><strong>Subject:</strong> Final <?php echo CONF_NAME; ?> presentation details</p>
	<hr>
	<p>Dear <?php echo $pre['name']; ?>,</p>
	<p>Thank you very much for volunteering to present at <?php echo CONF_NAME; ?>. We have finalized your presentation details and matched you with a moderator.</p>

	<p>Your moderator will be <?php echo $pre['moderators_name']; ?><?php if ($pre['moderators_pronouns'] != "") { echo " (" . $pre['moderators_pronouns'] . ")"; } ?>. Please email your moderator at <?php echo $pre['moderators_email']; ?> to arrange a time to discuss how you would like to be introduced.</p>

	<p>Your presentation time will be: <?php echo $presentation_time_utc; ?>. Please keep your presentation to 15 minutes, and allow the remainder of the hour for discussion.</p>

	<p>You can download your presentation appointment as an .ics file at the following link:</p>

	<p><?php echo SITE_URL; ?>programme/calendar.php?presenter=<?php echo $pre['id']; ?></p>

	<p>The following is the link to the web conference room where you will be presenting:</p>
	
	<p><?php echo $pre['hosting_details']; ?></p>

	<p>At the end of the presentation and discussion, your moderator will provide the participants with following link for feedback to be sent to the conference organizers:</p>

	<p><em>[Link will be inserted here]</em></p>

	<p>If you have any questions, please contact us: <?php echo SITE_URL; ?>contact/</p>
	<p>Best regards,</p>
	<p>The organization team<br><?php echo CONF_NAME; ?></p>
	<hr>
	<div class="alert alert-danger" role="alert">Warning: Sending an email cannot be undone</div>

	<form action="<?php echo SITE_URL; ?>admin/introduce-presenters-moderators.php" method="post">
	    <input type="hidden" name="action" value="email-presenter">
	    <input type="hidden" name="presenter" value="<?php echo $pre['id']; ?>">
	    <button class="btn btn-lg btn-success send-mod-confirm">Send email</button>
	    <button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	</form>
    </div>
<?php } ?>
<?php foreach ($presenters as $pre) { ?>
    <?php
    
    $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";
    
    ?>
    <div class="action-prompt" id="send-mod-intro-<?php echo $pre['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<h4>Send introduction email to <?php echo $pre['moderators_name']; ?></h4>
	<hr>
	<p><strong>To:</strong> <?php echo $pre['moderators_name']; ?> &lt;<?php echo $pre['moderators_email']; ?>&gt;</p>
	<p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>
	<p><strong>Subject:</strong> Final <?php echo CONF_NAME; ?> moderation details</p>
	<hr>
	<p>Dear <?php echo $pre['moderators_name']; ?>,</p>
	<p>Thank you very much for volunteering to moderate at <?php echo CONF_NAME; ?>. We have finalized your moderation details and matched you with a presenter.</p>

	<p>You will be moderating '<?php echo $pre['title']; ?>' by <?php echo $pre['name']; ?><?php if ($pre['pronouns'] != "") { echo " (" . $pre['pronouns'] . ")"; } ?>. Please email your presenter at <?php echo $pre['email']; ?> to arrange a time to discuss introducing the presenter and the topic.</p>

	<p>The presentation time will be: <?php echo $presentation_time_utc; ?>.</p>

	<p>You can download your moderation appointment as an .ics file at the following link:</p>

	<p><?php echo SITE_URL; ?>programme/calendar.php?presenter=<?php echo $pre['id']; ?></p>

	<p>The following is the link to the web conference room where you will be moderating:</p>
	
	<p><?php echo $pre['hosting_details']; ?></p>

	<p>Please familiarize yourself with the expectations for moderators at the following address:</p>
	<p><?php echo SITE_URL; ?>moderators/</p>

	<p>At the end of the presentation and discussion, please provide the participants with following link for feedback to be sent to the conference organizers:</p>

	<p><em>[Link will be inserted here]</em></p>

	<p>If you have any questions, please contact us: <?php echo SITE_URL; ?>contact/</p>
	<p>Best regards,</p>
	<p>The organization team<br><?php echo CONF_NAME; ?></p>
	<hr>
	<div class="alert alert-danger" role="alert">Warning: Sending an email cannot be undone</div>

	<form action="<?php echo SITE_URL; ?>admin/introduce-presenters-moderators.php" method="post">
	    <input type="hidden" name="action" value="email-moderator">
	    <input type="hidden" name="presenter" value="<?php echo $pre['id']; ?>">
	    <button class="btn btn-lg btn-success send-mod-confirm">Send email</button>
	    <button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	</form>
    </div>
<?php } ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
		<?php

		if ($_POST['action'] == "email-presenter") {
		    if (sch_send_presenter_intro_email ($_POST['presenter'])) {
			echo '<div class="alert alert-success" role="alert">Email sent</div>';
		    } else {
			echo '<div class="alert alert-danger" role="alert">Error sending email</div>';
		    }
		}

		if ($_POST['action'] == "email-moderator") {
		    if (sch_send_moderator_intro_email ($_POST['presenter'])) {
			echo '<div class="alert alert-success" role="alert">Email sent</div>';
		    } else {
			echo '<div class="alert alert-danger" role="alert">Error sending email</div>';
		    }
		}

		$sent_emails = sch_get_sent_intro_emails ();
		
		?>
	    </div>
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Introduce presenters and moderators</li>
		</ol>
	    </nav>
	    <h3>Introduce presenters and moderators</h3>

	    <div class="table-responsive mb-3">
		<table class="table table-striped table-sm">
		    <thead>
			<tr>
			    <td scope="col">
				Presenter
			    </td>
			    <td scope="col">
				Moderator
			    </td>
			    <td scope="col">
				Actions
			    </td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $pre) { ?>
			    <tr>
				<td>
				    <h4><?php echo $pre['name']; ?></h4>
				    <p class="text-muted">Email: <?php echo $pre['email']; ?></p>
				    <p class="text-muted">Handle: <?php echo $pre['handle']; ?></p>
				    <?php foreach ($sent_emails as $se) { if ($se['presenter'] == $pre['id'] & $se['role'] == "presenter") { ?>
					<span class="badge bg-success">Sent</span>
				    <?php } } ?>
				</td>
				<td>
				    <h4><?php echo $pre['moderators_name']; ?></h4>
				    <p class="text-muted">Email: <?php echo $pre['moderators_email']; ?></p>
				    <p class="text-muted">Handle: <?php echo $pre['moderators_handle']; ?></p>
				    <?php foreach ($sent_emails as $se) { if ($se['presenter'] == $pre['id'] & $se['role'] == "moderator") { ?>
					<span class="badge bg-success">Sent</span>
				    <?php } } ?>
				</td>
				<td>
				    <button class="btn btn-primary btn-sm mb-3 show-presenter-intro-prompt-button" data-pre="<?php echo $pre['id']; ?>">Email presenter</button>
				    <button class="btn btn-primary btn-sm mb-3 show-moderator-intro-prompt-button" data-mod="<?php echo $pre['id']; ?>">Email moderator</button>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
	    
	</div>
    </div>
</div>

<?php include (ABS_PATH . "footer.php"); ?>
