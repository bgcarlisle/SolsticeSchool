<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("confirmed-with-confirmed-mods");

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
	    <?php

	    if ($_POST['action'] == "save") {
		sch_update_presentation_link ($_POST['newlink']);
	    }

	    ?>
	    </div>
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Review web conference room links and final schedule</li>
		</ol>
	    </nav>
	    <h3>Review web conference room links and final schedule</h3>

	    <p>This page contains only presentations that have confirmed moderators.</p>

	    <p><a href="<?php echo SITE_URL; ?>admin/final-detailed-schedule.php">Download final detailed schedule as a .csv file</a></p>
	    
	    <div class="table-responsive">
		<table class="table table-striped">
		    <thead>
			<tr>
			    <th scope="col">
				Presenter
			    </th>
			    <th scope="col">
				Moderator
			    </th>
			    <th scope="col">
				Link
			    </th>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $pre) { ?>
			    <tr>
				<td>
				    <?php if ($pre['able_to_host'] == 1) { ?>
					<span class="badge bg-success" style="float: right;">Prefers to host</span>
				    <?php } else { ?>
					<span class="badge bg-secondary" style="float: right;">Does not prefer to host</span>
				    <?php } ?>
				    <h4><?php echo $pre['name']; ?></h4>
				    <p class="text-muted">Email: <?php echo $pre['email']; ?></p>
				    <p class="text-muted">Handle: <?php echo $pre['handle']; ?></p>
				</td>
				<td>
				    <h4><?php echo $pre['moderators_name']; ?></h4>
				    <p class="text-muted">Email: <?php echo $pre['moderators_email']; ?></p>
				    <p class="text-muted">Handle: <?php echo $pre['moderators_handle']; ?></p>
				</td>
				<td>
				    <div class="input-group mb-3">
					<input type="text" class="form-control link_review_input" id="link_input_<?php echo $pre['id']; ?>" placeholder="Link to web conference room" value="<?php echo $pre['hosting_details']; ?>" data-presenter="<?php echo $pre['id']; ?>">
					<input type="hidden" id="saved_link_<?php echo $pre['id']; ?>" value="<?php echo $pre['hosting_details']; ?>">
					<button id="save_link_button_<?php echo $pre['id']; ?>" class="btn btn-primary save_link_button" type="button" data-presenter="<?php echo $pre['id']; ?>" disabled>Save</button>
				    </div>
				    <div id="save_link_feedback_<?php echo $pre['id']; ?>" class="alert" role="alert" style="display: none;"></div>
				</td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
	</div>
    </div>
</div>

<?php include (ABS_PATH . "footer.php"); ?>
