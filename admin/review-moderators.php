<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$mods = sch_get_moderators ();

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Review moderator applications</li>
		</ol>
	    </nav>
	    <h3>Review moderator applications</h3>

	    <p>Moderators are approved by default and will appear on the page for matching moderators to presenters immediately after signup; this page exists so that if a prospective moderator withdraws, or if you discover that they would be a bad fit with the conference, you can take them off the moderator matching page. Rejection here does not contact them by email; you will need to communicate with them through other means.<p>

	    <?php if (count($mods) > 0) { ?>
		<?php foreach ($mods as $mod) { ?>
		    <?php

		    if ($mod['moderator_accepted'] == 1) {
			$status_class = "success";
			$status_text = "Approved";
		    } else {
			$status_class = "danger";
			$status_text = "Rejected";
		    }
		    ?>
		    <div class="card mb-3">
			<div class="card-body">
			    <span id="status_indicator_<?php echo $mod['id']; ?>" class="badge bg-<?php echo $status_class; ?>" style="float: right;"><?php echo $status_text; ?></span>
			    <input type="hidden" id="moderator_status_<?php echo $mod['id']; ?>" value="<?php echo $mod['moderator_accepted']; ?>">
			    <h4 class="card-title"><?php echo sch_format_text($mod['name']); ?><?php if ($mod['pronouns'] != "") { echo " (" . $mod['pronouns'] . ")"; } ?></h4>
			    <h5 class="text-muted">Email: <?php echo $mod['email'] ?></h5>
			    <h5 class="text-muted">Handle: <?php echo $mod['handle'] ?></h5>
			    <hr>
			    <div class="form-group">
				<label for="status_select_<?php echo $mod['id']; ?>">Moderator status</label>
				<div class="input-group mb-3">
				    <select class="form-control mod_accept_status_select" data-mod="<?php echo $mod['id']; ?>" id="status_select_<?php echo $mod['id']; ?>">
					<option value="1"<?php if ($mod['moderator_accepted'] == 1) { echo " selected"; } ?>>Accepted</option>
					<option value="0"<?php if ($mod['moderator_accepted'] != 1) { echo " selected"; } ?>>Rejected</option>
				    </select>
				    <button id="save_moderator_accept_<?php echo $mod['id']; ?>" class="btn btn-primary save_mod_accept" type="button" data-mod="<?php echo $mod['id']; ?>" disabled>Save</button>
				</div>
			    </div>
			</div>
		    </div>
		<?php } ?>
	    <?php } else { ?>
		<div class="alert alert-warning" role="alert">
		    No moderators have signed up yet
		</div>
	    <?php } ?>
	    
	</div>
    </div>
</div>


<?php

include (ABS_PATH . "footer.php");

?>
