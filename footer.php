
<!-- jQuery -->
<script src="<?php echo SITE_URL; ?>/js/jquery-3.5.1.min.js"></script>

<!-- Bootstrap JS -->
<script src="<?php echo SITE_URL; ?>/js/bootstrap.min.js"></script>

<!-- Here goes any custom Summer School JS -->
<script>
 siteurl = '<?php echo SITE_URL; ?>';

 $(document).ready(function () {

     // What happens when you click a checkbox on the presenter signup page table
     // (Copy the checked cells into a hidden input)
     $(document).on('click', '.availability_check', function () {
	 // Update the hidden element
	 checked_avail = [];
	 $('.availability_check:checked').each(function() {
	     checked_avail.push($(this).val());
	 });
	 $('#checked_slots').val(checked_avail.toString());
	 
	 // Make the button disabled or not
	 if ($('#checked_slots').val() == '') {
	     $('#send_presenter_application').prop("disabled", true);
	 } else {
	     $('#send_presenter_application').prop("disabled", false);
	 }
     });
     // End of what happens when you click a checkbox on the presenter signup page table

     // If we're on the presenter confirmation page, load up the already checked slots
     if ($('#presenter-confirm-button').length == 1) {
	 alreadychecked = $('#checked_slots').val();
	 checked_slots = alreadychecked.split(",");
	 checked_slots.forEach(function (slot) {
	     $('#avail_check_' + slot).prop('checked', true);
	 });
     }
     // End of if we're on the presenter confirmation page

     // If we're on the mod signup page
     if ($('#mod-signup-button').length == 1) {

	 // When to make the button active
	 $('input.form-check-input').click(function () {
	     if ($('#mod_policy:checked').length == 1 & $('.mod-signup-time-checkbox:checked').length > 0) {
		 $('#mod-signup-button').prop('disabled', false);
	     } else {
		 $('#mod-signup-button').prop('disabled', true);
	     }
	 });
	 
     }
     // End of mod signup page conditional

     // If we're on the participant signup page
     if ($('#partic-signup-button').length == 1) {
	 $('input.form-check-input, #partic_email').change(function () {
	     if ($('#partic_policy:checked').length == 1 & $('.partic-signup-time-checkbox:checked').length > 0 & $('#partic_email').val() != '') {
		 $('#partic-signup-button').prop('disabled', false);
	     } else {
		 $('#partic-signup-button').prop('disabled', true);
	     }
	 });
     }
     // End of participant signup conditional

     // What happens when you check that a mod is available at a certain time
     $('.mod-signup-time-checkbox').change(function () {
	 checked_avail = [];
	 $('.mod-signup-time-checkbox:checked').each(function () {
	     checked_avail.push($(this).data('slot'));
	 });
	 $('#checked_presentations').val(checked_avail.toString())
     });
     // End of what happens when you check that a mod is available at a certain time

     // What happens when you check that you want to attend a presentation
     $('.partic-signup-time-checkbox').change(function () {
	 checked_avail = [];
	 $('.partic-signup-time-checkbox:checked').each(function () {
	     checked_avail.push($(this).data('slot'));
	 });
	 $('#checked_presentations').val(checked_avail.toString()).trigger('change');
	 $('#download-checked-presentations-link a').prop('href', siteurl + 'programme/calendar-checked.php?presentations=' + checked_avail.toString());
	 if (checked_avail.toString() == '') {
	     $('#download-checked-presentations-link').slideUp();
	 } else {
	     $('#download-checked-presentations-link').slideDown();
	 }
     });
     // End of what happens when you check a presentation to attend
     
     // What happens when you choose a new time zone on the presenter signup page
     $('#tz_selector, #presenter_confirm_tz_selector').change(function () {

	 tz_offset = $(this).val();

	 $('#avail_table_change_feedback').slideDown(0).html('Changing time zone ...');
	 
	 $.ajax ({
	     url: siteurl + 'presenters-signup/availability-table.php',
	     type: 'post',
	     data: {
		 tzoffset: tz_offset
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     $('#availability_table').html(response);
	     // Then add the checks back
	     alreadychecked = $('#checked_slots').val();
	     checked_slots = alreadychecked.split(",");
	     checked_slots.forEach(function (slot) {
		 $('#avail_check_' + slot).click();
	     });

	     setTimeout(function () {
		 $('#avail_table_change_feedback').slideUp();
	     }, 500);

	 });
     });
     // End of what happens when you choose a new time zone on the presenter signup page

     // What happens when you change the acceptance status selector
     // (Will it let you click save, or will it remind you that was the status already?)
     $('.presenter_accept_status_select').change(function () {

	 pid = $(this).data('presenter');

	 if ($(this).val() != $('#presenter_status_' + pid).val()) {
	     $('#save_presenter_' + pid).prop('disabled', false);
	 } else {
	     $('#save_presenter_' + pid).prop('disabled', true);
	 }
	 
     });
     // End of what happens when you change the acceptance status selector

     // What happens when you change the acceptance status selector for mods
     // (Will it let you click save, or will it remind you that was the status already?)
     $('.mod_accept_status_select').change(function () {

	 mid = $(this).data('mod');

	 if ($(this).val() != $('#moderator_status_' + mid).val()) {
	     $('#save_moderator_accept_' + mid).prop('disabled', false);
	 } else {
	     $('#save_moderator_accept_' + mid).prop('disabled', true);
	 }
	 
     });
     // End of what happens when you change the acceptance status selector

     // What happens when you click "save" on the page for accepting presentation applications
     $('.save_acceptance_status').click(function () {
	 
	 pid = $(this).data('presenter');
	 $('#save_presenter_' + pid).prop('disabled', true);
	 newstatus = $('#status_select_' + pid).val();

	 $.ajax ({
	     url: siteurl + 'admin/save-acceptance-status.php',
	     type: 'post',
	     data: {
		 pid: pid,
		 status: newstatus
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == 1) { // Successful update

		 $('#presenter_status_' + pid).val(newstatus);

		 switch (newstatus) {
		     case "0":
			 $('#status_indicator_' + pid).removeClass('bg-success bg-danger').addClass('bg-secondary').html('Waiting');
			 break;
		     case "1":
			 $('#status_indicator_' + pid).removeClass('bg-secondary bg-danger').addClass('bg-success').html('Accepted');
			 break;
		     case "2":
			 $('#status_indicator_' + pid).removeClass('bg-secondary bg-success').addClass('bg-danger').html('Rejected');
			 break;
		     case "3":
			 $('#status_indicator_' + pid).removeClass('bg-secondary bg-success').addClass('bg-danger').html('Withdrawn');
			 break;
		 }
		 
	     } else { // Update failure
		 alert('Error updating status');
		 $('#save_presenter_' + pid).prop('disabled', false);
	     }
	     
	 });
	 
     });
     // End of what happens when you click "save" on the page for accepting presentation applications

     // What happens when you click "save" on the page for accepting mods
     $('.save_mod_accept').click(function () {
	 
	 mid = $(this).data('mod');
	 $('#save_moderator_accept_' + mid).prop('disabled', true);
	 newstatus = $('#status_select_' + mid).val();

	 $.ajax ({
	     url: siteurl + 'admin/save-mod-status.php',
	     type: 'post',
	     data: {
		 mid: mid,
		 status: newstatus
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == 1) { // Successful update

		 $('#moderator_status_' + mid).val(newstatus);

		 switch (newstatus) {
		     case "0":
			 $('#status_indicator_' + mid).removeClass('bg-success bg-danger').addClass('bg-danger').html('Rejected');
			 break;
		     case "1":
			 $('#status_indicator_' + mid).removeClass('bg-secondary bg-danger').addClass('bg-success').html('Accepted');
			 break;
		 }
		 
	     } else { // Update failure
		 alert('Error updating status');
		 $('#save_presenter_' + mid).prop('disabled', false);
	     }
	     
	 });
	 
     });
     // End of what happens when you click "save" on the page for accepting mods

     // What happens when a checkbox on the scheduler table is clicked
     $('table.presentation-sched input.form-check-input').click(function () {

	 $('#save_schedule_button').prop('disabled', false);

	 pid = $(this).data('presenter');
	 slot = $(this).data('slot');
	 
	 if ($(this).prop('checked') == false) { // Un-checking it
	     $('.presenter_' + pid).prop('disabled', false);
	     $('.presenter_sched_status_' + pid).addClass('table-warning').removeClass('table-success').html('Waiting');
	 } else { // Checking it
	     $('.presenter_' + pid).prop('disabled', true);
	     $(this).prop('disabled', false);
	     $('.presenter_sched_status_' + pid).addClass('table-success').removeClass('table-warning').html('Scheduled');
	 }

	 taken = $('.sched_slot_' + slot + ':checked').length;

	 switch (taken) {
	     case 0:
		 $('#taken_' + slot).removeClass('table-success table-warning table-danger').addClass('table-secondary').html('');
		 break;
	     case 1:
		 $('#taken_' + slot).removeClass('table-secondary table-warning table-danger').addClass('table-success').html(taken);
		 break;
	     case 2:
		 $('#taken_' + slot).removeClass('table-success table-secondary table-danger').addClass('table-warning').html(taken);
		 break;
	     default:
		 $('#taken_' + slot).removeClass('table-success table-secondary table-warning').addClass('table-danger').html(taken);
		 break;
	 }

	 sched = [];
	 $('.form-check-input:checked').each(function () {
	     pid = $(this).data('presenter');
	     slot = $(this).data('slot');
	     sched.push(pid + '-' + slot);
	 });
	 $('#schedule_selections').val(sched.toString());
	 
     });
     // End of what happens when a checkbox on the scheduler table is clicked

     // What happens when the action mask is clicked
     $('#action-mask, .close-prompt').click(function (event) {
	 event.preventDefault();
	 $('.action-prompt').slideUp(0, function () {
	     $('#action-mask').fadeOut();
	 });
     });
     // End of what happens when the action mask is clicked

     // What happens when you open the prompt to send an email to
     // confirm a presentation time
     $('.prompt-send-presenter-confirm').click(function () {
	 pid = $(this).data('presenter');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-presenter-confirm-' + pid).slideDown();
	 });
     });
     // End of presentation time confirmation email prompt

     // What happens when you open the prompt to send an email to
     // confirm a mod
     $('.prompt-send-mod-confirm').click(function () {
	 mid = $(this).data('mod');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-mod-confirm-' + mid).slideDown();
	 });
     });
     // End of mod confirmation email prompt

     // What happens when you open the prompt to send an email to
     // presenters to request presentation materials
     $('.prompt-send-presenter-materials').click(function () {
	 pid = $(this).data('presenter');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-presenter-request-' + pid).slideDown();
	 });
     });
     // End of presenter materials prompt

     // What happens when you click the change attendance plans button
     $('#partic-change-signup-button').click(function () {
	 $('#action-mask').fadeIn(500, function () {
	     $('#change-participant-signups-dialog').slideDown();
	 });
     });
     // End of change attendance plans button

     // What happens when you open the prompt to clear a confirmed
     // presentation time or active link
     $('.prompt-clear-presenter-link').click(function () {
	 pid = $(this).data('presenter');
	 $('#action-mask').fadeIn(500, function () {
	     $('#clear-presenter-confirm-' + pid).slideDown();
	 });
     });
     // End of clear button prompt

     // What happens when you open the prompt to clear a confirmed
     // moderator
     $('.prompt-clear-mod-link').click(function () {
	 mid = $(this).data('mod');
	 $('#action-mask').fadeIn(500, function () {
	     $('#clear-mod-confirm-' + mid).slideDown();
	 });
     });
     // End of clear moderator prompt
     
     // What happens when you click "send" in the contact form
     $('#send-contact-form').click(function () {
	 $(this).prop('disabled', true);
	 $.ajax ({
	     url: siteurl + 'contact/send.php',
	     type: 'post',
	     data: {
		 email: $('#contact_email').val(),
		 message: $('#contact_text').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     $('#contact_form').slideUp(500, function() {
		 $('#contact_form').html(response).slideDown();
	     });   
	 });
     });
     // End of contact form "send"

     // What happens when you save your attendance plans
     $('#partic-signup-button').click(function () {
	 $('#participant-signup-form-container input, #participant-signup-form-container button, #participant-signup-form-container select').prop('disabled', true);
	 $.ajax ({
	     url: siteurl + 'programme/save-participant.php',
	     type: 'post',
	     data: {
		 email: $('#partic_email').val(),
		 handle: $('#partic_handle').val(),
		 presentations: $('#checked_presentations').val(),
		 policy: $('#partic_policy:checked').length
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     $('#participant-signup-form-container').slideUp(500, function() {
		 $('#participant-signup-form-container').html(response).slideDown();
	     });   
	 });
     });
     // End of what happens when you save your attendance plans

     // What happens if you go to the presenter confirm page and say that
     // you're not available to present at this time
     $('.confirm-presentation-time').change(function () {
	 if ($('#confirm-presentation-time-yes').prop('checked')) {
	     $('#confirm-details-and-presentation-container').slideDown();
	     $('#confirm-presentation-cant-present-container').slideUp();
	 }
	 if ($('#confirm-presentation-time-no').prop('checked')) {
	     $('#confirm-details-and-presentation-container').slideUp();
	     $('#confirm-presentation-cant-present-container').slideDown();
	 }
	 if ($('#confirm-presentation-time-never').prop('checked')) {
	     $('#confirm-details-and-presentation-container').slideUp();
	     $('#confirm-presentation-cant-present-container').slideUp();
	 }
	 if ($('.confirm-presentation-time').length > 0) {
	     $('#presenter-confirm-button').prop('disabled', false);
	 }
     });
     // End of presenter confirm radio box selection

     // What happens when you change time zones on the presenter confirm
     $('#presenter_confirm_tz_selector').change(function () {
	 slot = $('#confirm_assigned_slot').val();
	 conf_start = <?php echo strtotime(CONF_START); ?> * 1000;
	 tz_offset = $('#presenter_confirm_tz_selector').val();
	 slot_ms = conf_start + ((parseInt(slot)-1) * 60 * 60 * 1000) + parseInt(tz_offset) * 1000;
	 tz_date = new Date(slot_ms);
	 weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
	 formatted_date = tz_date.getUTCFullYear() + '-' + ('0' + (tz_date.getUTCMonth()+1)).slice(-2) + '-' + ('0' + tz_date.getUTCDate()).slice(-2) + ' (' + weekdays[tz_date.getUTCDay()] + ') ' + ('0' + tz_date.getUTCHours()).slice(-2) + ':' + ('0' + tz_date.getUTCMinutes()).slice(-2);
	 
	 $('.assigned_slot_text').html(formatted_date);
     });
     // End of time zone change on presenter confirm

     // What happens when you change time zones on the moderator signup
     $('#mod_signup_tz_selector, #partic_signup_tz_selector, #mod_confirm_tz_selector').change(function () {
	 tz_offset = $(this).val();
	 conf_start = <?php echo strtotime(CONF_START); ?> * 1000;
	 weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
	 
	 $('.signup_time').each(function() {
	     slot = $(this).data('slot');
	     slot_ms = conf_start + ((parseInt(slot)-1) * 60 * 60 * 1000) + parseInt(tz_offset) * 1000;
	     tz_date = new Date(slot_ms);
	     formatted_date = tz_date.getUTCFullYear() + '-' + ('0' + (tz_date.getUTCMonth()+1)).slice(-2) + '-' + ('0' + tz_date.getUTCDate()).slice(-2) + ' (' + weekdays[tz_date.getUTCDay()] + ') ' + ('0' + tz_date.getUTCHours()).slice(-2) + ':' + ('0' + tz_date.getUTCMinutes()).slice(-2);
	     $(this).html(formatted_date);
	 });
     });
     // End of what happens when you change time zones on the moderator signup

     // What happens when you click the "I can host my own" on the presenter confirm page
     $('#confirm_presenter_host').click(function () {
	 if ($('#confirm_presenter_host').prop('checked')) {
	     $('#hosting_details_container').slideDown();
	 } else {
	     $('#hosting_details_container').slideUp();
	 }
     });
     // End of the "I can host my own" toggle

     // What happens when the presenter clicks the "confirm button"
     $('#presenter-confirm-button').click(function () {
	 $('input, button, textarea, select').prop('disabled', true);

	 $.ajax ({
	     url: siteurl + 'presenters-signup/save-presenter-confirmation.php',
	     type: 'post',
	     data: {
		 link: $('#presenter-confirm-button').data('link'),
		 confirm_time: $('.confirm-presentation-time:checked').val(),
		 availability: $('#checked_slots').val(),
		 name: $('#presenter_name').val(),
		 pronouns: $('#presenter_pronouns').val(),
		 handle: $('#presenter_handle').val(),
		 title: $('#presenter_title').val(),
		 abstract: $('#presenter_abstract').val(),
		 host: $('#confirm_presenter_host:checked').length,
		 hosting_details: $('#presenter_hosting_details').val(),
		 recording: $('#presenter_recording:checked').length,
		 transcript: $('#provide_transcript:checked').length
	     },
	     dataType: 'html'
	 }).done( function (response) {
	     $('#presentation-details-container').slideUp(500, function () {
		 $('#presentation-details-container').html(response).slideDown();
	     });
	 });
	 
     });
     // End of "confirm" button

     // Remove the stuff after five seconds
     setTimeout(function () {
	 $('.five-second-removal').slideUp();
     }, 5000);
     // End of remove stuff

     // What happens when you click the presenter signup button
     $('#send_presenter_application').click(function () {
	 
	 $.ajax ({
	     url: siteurl + 'presenters-signup/send.php',
	     type: 'post',
	     data: {
		 name: $('#presenter_name').val(),
		 pronouns: $('#presenter_pronouns').val(),
		 handle: $('#presenter_handle').val(),
		 email: $('#presenter_email').val(),
		 title: $('#presenter_title').val(),
		 abstract: $('#presenter_abstract').val(),
		 host: $('#presenter_host:checked').length,
		 recording: $('#presenter_recording:checked').length,
		 transcript: $('#provide_transcript:checked').length,
		 availability: $('#checked_slots').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     $('#presenter_form').slideUp(500, function() {
		 $('#presenter_form').html(response).slideDown();
	     });
	     
	 });
	 
     });
     // End of what happens happens when you click the presenter signup button

     // What happens when you click the moderator signup button
     $('#mod-signup-button').click(function () {
	 
	 $.ajax ({
	     url: siteurl + 'moderators-signup/send.php',
	     type: 'post',
	     data: {
		 name: $('#mod_name').val(),
		 pronouns: $('#mod_pronouns').val(),
		 handle: $('#mod_handle').val(),
		 email: $('#mod_email').val(),
		 max_mods: $('#mod_number').val(),
		 host: $('#mod_host:checked').length,
		 availability: $('#checked_presentations').val(),
		 policy: $('#mod_policy:checked').length
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     $('#moderator_form').slideUp(500, function() {
		 $('#moderator_form').html(response).slideDown();
	     });
	     
	 });
	 
     });
     // End of what happens happens when you click the moderator signup button

     // What happens when you click save changes on the participant view/edit page
     $('#partic-save-changes-button').click(function () {
	 $.ajax ({
	     url: siteurl + 'programme/edit.php',
	     type: 'post',
	     data: {
		 link: '<?php echo $_GET['link']; ?>',
		 participant: $('#participant_id').val(),
		 availability: $('#checked_presentations').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {
	     if (response == '1') {
		 // Feedback
		 $('#partic-save-changes-feedback').slideDown(500, function () {
		     setTimeout(function () {
			 $('#partic-save-changes-feedback').slideUp();
		     }, 5000);
		 });
		 // button
		 $('#partic-save-changes-button').prop('disabled', true);
		 // hidden
		 $('#checked_presentations_saved').val($('#checked_presentations').val());
	     }
	 });
     });
     // End of what happens when you click save changes on the participant view/edit page

     // What happens when the checked presentations change on the view/edit presentation page
     $('#checked_presentations').change(function () {
	 if ($('#partic-save-changes-button').length > 0) {
	     if ($('#checked_presentations').val() == $('#checked_presentations_saved').val()) {
		 $('#partic-save-changes-button').prop('disabled', true);
	     } else {
		 $('#partic-save-changes-button').prop('disabled', false);
	     }
	 }	 
     });
     // End of what happens when the checked presentations change on the view/edit presentation page

     // What happens when you send yourself the participant link by email
     $('#send-participant-change').click(function () {
	 $('#send-participant-change-email-inner input button').prop('disabled', true);
	 
	 $.ajax ({
	     url: siteurl + 'programme/resend-email.php',
	     type: 'post',
	     data: {
		 email: $('#partic_change_email').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {
	     $('#send-participant-change-email-inner').slideUp(500, function() {
		 $('#send-participant-change-email-inner').html(response).slideDown();
	     });
	 });
     });
     // End of what happens when you send yourself the participant link by email

     // What happens when you click a checkbox on the match-mods page
     $('.match-mods-check').click(function () {
	 presenter = $(this).data('presenter');
	 moderator = $(this).data('moderator');
	 

	 if ($(this).prop('checked') == false) { // Un-checking it
	     $('.match-mods-check-pre-' + presenter).prop('disabled', false);
	     $('.presenter_match_status_' + presenter).addClass('table-warning').removeClass('table-success').html('Waiting');
	 } else { // Checking it
	 $('.match-mods-check-pre-' + presenter + ':not(.match-mods-check-mod-' + moderator + ')').prop('disabled', true);
	     $('.presenter_match_status_' + presenter).addClass('table-success').removeClass('table-warning').html('Matched');
	 }

	 modmatchnum = $('.match-mods-check-mod-' + moderator + ':checked').length;
	 modmatchden = $('#mod-number-matched-' + moderator).data('maxmods');
	 
	 $('#mod-number-matched-' + moderator).html(modmatchnum);
	 if (modmatchnum <= modmatchden) {
	     $('#mod-number-matched-cell-' + moderator).addClass('table-success').removeClass('table-danger');
	 }
	 
	 if (modmatchnum > modmatchden) {
	     $('#mod-number-matched-cell-' + moderator).addClass('table-danger').removeClass('table-success');
	 }

	 if (modmatchnum == 0) {
	     $('#mod-number-matched-cell-' + moderator).removeClass('table-danger').removeClass('table-success');     
	 }


	 matches = [];
	 $('.match-mods-check:checked').each(function () {
	     pid = $(this).data('presenter');
	     mid = $(this).data('moderator');
	     matches.push(pid + '-' + mid);
	 });
	 $('#matched-mod-selections').val(matches.toString());
	 
	 $('#save-matched-mods-button').prop('disabled', false);
	 
     });
     // End of what happens when you click a checkbox on the match-mods page

     // What happens when you click "save changes" on the mod-match page
     $('#save-matched-mods-button').click(function () {
	 $(this).prop('disabled', true);

	 $.ajax ({
	     url: siteurl + 'admin/save-matched-mods.php',
	     type: 'post',
	     data: {
		 schedule: $('#matched-mod-selections').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == "1") {
		 $('#matched_mods_save_feedback').html('Moderator matches saved').addClass('alert-success').removeClass('alert-danger').slideDown();
	     } else {
		 $('#matched_mods_save_feedback').html('Error saving moderator matches').addClass('alert-danger').removeClass('alert-success').slideDown();
		 $('#save-matched-mods-button').prop('disabled', false);
	     }

	     setTimeout(function () {
		 $('#matched_mods_save_feedback').slideUp();
	     }, 1000);
	     
	 });
	 
     });
     // End of what happens when you click "save changes" on the mod-match page

     // What happens when you click a checkbox on the mod confirm page
     $('.mod-confirm-time-checkbox').click(function () {
	 pid = $(this).data('presenter');
	 if ($(this).prop('checked') == true) { // Checking it
	     $('#mod-link-input-container-' + pid).slideDown();
	 } else { // Un-checking it
	     $('#mod-link-input-container-' + pid).slideUp();
	     $('#mod-link-input-container-' + pid + ' input').val('');
	 }

	 checked = [];
	 $('.mod-confirm-time-checkbox:checked').each(function () {
	     pid = $(this).data('presenter');
	     checked.push(pid);
	 });
	 $('#checked_presentations').val(checked.toString());

	 if ($('.mod-confirm-time-checkbox:checked').length > 0) {
	     $('#mod-multiple-calendar-dl').slideDown();
	     $('#mod-multiple-calendar-dl a').prop('href', siteurl + 'programme/calendar-checked.php?presentations=' + $('#checked_presentations').val());
	 } else {
	     $('#mod-multiple-calendar-dl').slideUp();
	 }
	 
     });
     // End of what happens when you click a checkbox on the mod confirm page

     // What happens when you click the "I agree" button on the mod confirm page
     $('#mod-confirm-policy').click(function () {
	 if ($(this).prop('checked') == true) { // Checking it
	     $('#save-mod-confirm-button').prop('disabled', false);
	 } else {
	     $('#save-mod-confirm-button').prop('disabled', true);	     
	 }
     });
     // End of what happens when you click the "I agree" button on the mod confirm page

     // What happens when you click the save mod confirm button
     $('#save-mod-confirm-button').click(function () {
	 datatosend = $('form').serialize();
	 
	 $('input, button, select').prop('disabled', true);
	 
	 $.ajax ({
	     url: siteurl + 'moderators-signup/save-moderator-confirmation.php',
	     type: 'post',
	     data: datatosend,
	     dataType: 'html'
	 }).done( function (response) {
	     $('#moderation-details-container').slideUp(500, function () {
		 $('#moderation-details-container').html(response).slideDown();
	     });
	 });
     });
     // End of what happens when you click the save mod confirm button

     // What happens when you click the "save schedule" button
     $('#save_schedule_button').click(function () {
	 
	 $('#save_schedule_button').prop('disabled', true);
	 
	 $.ajax ({
	     url: siteurl + 'admin/save-schedule.php',
	     type: 'post',
	     data: {
		 schedule: $('#schedule_selections').val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == "1") {
		 $('#schedule_save_feedback').html('Schedule saved').addClass('alert-success').removeClass('alert-danger').slideDown();
	     } else {
		 $('#schedule_save_feedback').html('Error saving schedule').addClass('alert-danger').removeClass('alert-success').slideDown();
		 $('#save_schedule_button').prop('disabled', false);
	     }

	     setTimeout(function () {
		 $('#schedule_save_feedback').slideUp();
	     }, 1000);
	     
	 });
     });
     // End of what happens when you click the "save schedule" button

     // What happens when you change the link input
     $('.link_review_input').on("change keyup blur", function () {
	 pid = $(this).data('presenter');
	 
	 if ($('#link_input_' + pid).val() == $('#saved_link_' + pid).val()) {
	     $('#save_link_button_' + pid).prop('disabled', true);
	 } else {
	     $('#save_link_button_' + pid).prop('disabled', false);
	 }
     });
     // End of what happens when you change the link

     // What happens when you save the link
     $('.save_link_button').click(function () {

	 pid = $(this).data('presenter');
	 
	 $(this).prop('disabled', true);
	 
	 $.ajax ({
	     url: siteurl + 'admin/save-link.php',
	     type: 'post',
	     data: {
		 pid: pid,
		 link: $('#link_input_' + pid).val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == "1") {
		 $('#save_link_feedback_' + pid).html('Saved').addClass('alert-success').removeClass('alert-danger').slideDown();
		 $('#saved_link_' + pid).val($('#link_input_' + pid).val());
	     } else {
		 $('#save_link_feedback_' + pid).html('Error saving').addClass('alert-danger').removeClass('alert-success').slideDown();
		 $('#save_link_button_' + pid).prop('disabled', false);
	     }

	     setTimeout(function () {
		 $('#save_link_feedback_' + pid).slideUp();
	     }, 1000);
	     
	 });
	 
     });
     // End of what happens when you save the link

     // What happens when you click the show presenter or moderator prompt button
     $('.show-moderator-intro-prompt-button').click(function () {
	 pid = $(this).data('mod');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-mod-intro-' + pid).slideDown();
	 });
     });
     $('.show-presenter-intro-prompt-button').click(function () {
	 pid = $(this).data('pre');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-pre-intro-' + pid).slideDown();
	 });
     });
     // End of what happens when you click the show presenter or moderator prompt button

     // What happens when you click the "Email" button on the invite participants page
     $('.prompt-email-participants-button').click(function () {
	 pid = $(this).data('presenter');
	 $('#action-mask').fadeIn(500, function () {
	     $('#send-participant-invite-' + pid).slideDown();
	 });
     });
     // End of what happens when you click the "Email" button on the invite participants page

     // What happens when you click "Save" on a participant status
     $('.save-participant-signup-status').click(function () {
	 pid = $(this).data('participant');
	 newstatus = $('#participant-signup-status-select-' + pid).val();

	 $(this).prop('disabled', true);

	 $.ajax ({
	     url: siteurl + 'admin/save-participant-status.php',
	     type: 'post',
	     data: {
		 pid: pid,
		 newstatus: newstatus
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response == "1") {
		 $('#participant-save-status-feedback-' + pid).html('New status saved').addClass('alert-success').removeClass('alert-danger').slideDown();
	     } else {
		 $('#participant-save-status-feedback-' + pid).html('Error saving').addClass('alert-danger').removeClass('alert-success').slideDown();
	     }

	     setTimeout(function () {
		 $('#participant-save-status-feedback-' + pid).slideUp();
	     }, 1000);
	     
	 });
	 
     });
     // End of what happens when you click "Save" on a participant status

     // What happens when you change the participant status dropdown
     $('.participant-signup-status-selector').change(function () {
	 $(this).parent().children('button').prop('disabled', false);
     });
     // End of what happens when you change the participant status dropdown

     // What happens when you change the "hidden by admin" status on the archive admin page
     $('.presenter-materials-status-selector').change(function () {
	 pid = $(this).data('presenter');
	 current_status = $(this).data('status');
	 new_status = $(this).val();

	 if (parseInt(current_status) == parseInt(new_status)) {
	     $('#save-presenter-materials-status-' + pid).prop('disabled', true);
	 } else {
	     $('#save-presenter-materials-status-' + pid).prop('disabled', false);
	 }
	 
     });
     // End of change "hidden by admin"

     // What happens when you click "save" on the "hidden by admin" status changer
     // for the presentation materials page
     $('.save-presenter-materials-status').click(function () {
	 pid = $(this).data('presenter');
	 
	 $.ajax ({
	     url: siteurl + 'admin/save-presenter-archive-hidden-status.php',
	     type: 'post',
	     data: {
		 pid: pid,
		 newval: $('#presenter-materials-status-select-' + pid).val()
	     },
	     dataType: 'html'
	 }).done( function (response) {

	     if (response != "Error") {
		 $('#save-archive-hidden-feedback-' + pid).addClass('alert-success').html('Saved').slideDown();
		 $('#presenter-materials-status-select-' + pid).data('status', response);
		 $('#save-presenter-materials-status-' + pid).prop('disabled', true);
	     } else {
		 $('#save-archive-hidden-feedback-' + pid).addClass('alert-danger').html('Database error saving status').slideDown();
	     }

	     setTimeout(function () {
		 $('#save-archive-hidden-feedback-' + pid).slideUp();
	     }, 1000);
	     
	 });
     });
     // End of save-click for "hidden by admin"

     // What happens when you click to move presentation materials up or down
     $('#materials-container').on('click', '.mat-move-up', function (event) {
	 event.preventDefault();
	 $(this).parent().parent().prev().insertAfter($(this).parent().parent());
     });
     $('#materials-container').on('click', '.mat-move-down', function (event) {
	 event.preventDefault();
	 $(this).parent().parent().next().insertBefore($(this).parent().parent());
     });
     // End of moving presentation materials up

     // What happens when you click to remove presentation materials
     $('#materials-container').on('click', '.mat-remove', function (event) {
	 event.preventDefault();
	 $(this).parent().parent().slideUp(500, function () {
	     $(this).remove();
	 });
     });
     // End of removal of presentation materials

     // Adding a link to the archive
     $('#add-link-to-materials').click(function (event) {
	 event.preventDefault();
	 template = $('#link-materials-template').html();

	 matid = 0;
	 
	 while ($('#materials_title_' + matid).length > 0) {	     
	     matid++;
	 }

	 template = template.replace(/\[matid\]/g, matid);
	 
	 $('#materials-container').append('<div class="card mb-3" style="display: none;">' + template + '</div>').children().slideDown();
     });
     // End of adding a link to the archive

     // Adding text to the archive
     $('#add-text-to-materials').click(function (event) {
	 event.preventDefault();
	 template = $('#text-materials-template').html();

	 matid = 0;
	 
	 while ($('#materials_title_' + matid).length > 0) {	     
	     matid++;
	 }

	 template = template.replace(/\[matid\]/g, matid);
	 
	 $('#materials-container').append('<div class="card mb-3" style="display: none;">' + template + '</div>').children().slideDown();
     });
     // End of adding a link to the archive

     // Show file upload prompt
     $('#add-file-to-materials-prompt').click(function (event) {
	 event.preventDefault();
	 $('#action-mask').fadeIn(500, function () {
	     $('#upload-file-prompt').slideDown();
	 });
     });
     // End of show file upload prompt

     // What happens when a file is selected
     $('#file-upload-selector').change(function () {
	 $('#add-file-to-materials').prop('disabled', false);
     });
     // End of what happens when a file is selected

     // What happens when you click to upload the file
     $('#file-upload-form').on('submit', function (event) {
	 event.preventDefault();

	 $('#file-upload-form').slideUp(0);
	 $('#file-upload-progress').slideDown();

	 $.ajax({
	     url: siteurl + 'archive/upload-file.php',
	     type: 'post',
	     data: new FormData(this),
	     contentType: false,
	     cache: false,
	     processData: false
	 }).done(function (response) {
	     response = JSON.parse(response);
	     $('#file-upload-progress').slideUp();
	     
	     if (response[0] == "1") {
		 // Add a file element to the editor

		 console.log(response[1]);

		 template = $('#file-materials-template').html();
		 
		 matid = 0;
		 
		 while ($('#materials_title_' + matid).length > 0) {	     
		     matid++;
		 }

		 template = template.replace(/\[matid\]/g, matid);
		 template = template.replace(/\[matcontent\]/g, response[1]);

		 
		 $('.action-prompt').slideUp(0, function () {
		     $('#action-mask').fadeOut(500, function () {
			 $('#materials-container').append('<div class="card mb-3" style="display: none;">' + template + '</div>').children().slideDown();
			 $('#file-upload-form').slideDown(0);
			 $('#file-upload-selector').val('');
		     });
		 });
		 
	     } else {

		 // Error
		 $('#file-upload-feedback').html(response[1]).slideDown(500, function () {
		     setTimeout(function () {
			 $('#file-upload-feedback').slideUp();
			 $('#file-upload-form').slideDown();
		     }, 5000);
		 });
	     }
	 });
     });
     // End of what happens when you click to upload the file

 });

</script>

</body>
</html>
