<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {
    $presenter_form_avail = TRUE;
} else {
    $presenter_form_avail = FALSE;
}

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {
    $mod_form_avail = TRUE;
} else {
    $mod_form_avail = FALSE;
}

if (time() <= strtotime(CONF_END)) {
    $partic_form_avail = TRUE;
} else {
    $partic_form_avail = FALSE;
}

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>Information for participants</h3>

	    <p>It is an expectation that participants follow these rules:</p>

	    <ul>
		<li>No vocal interruption of the presenter</li>
		<li>Hold all questions and comments until the end</li>
		<li>Use the text chat</li>
		<li>Turn off your camera and microphone until the end of the talk</li>
	    </ul>

	    <p> Because the talks are going to be relatively short, we are operating on a one strike rule. If you make a comment that is offensive or out of line, you will be muted or kicked out.</p>

	    <p>Closed captions will be available for all talks, unless the presenter has opted to provide their own videoconferencing room. If a speaker chooses to record the talk, we will offer to publish it later with a transcript. Talks will not be recorded without participants' consent.</p>

	    <?php if ($presenter_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>presenters-signup/">signup form for presenters</a> is open from <?php echo PRESENT_SIGNUP_START; ?> to <?php echo PRESENT_SIGNUP_END; ?>.</p>
	    <?php } ?>
	    
	    <?php if ($mod_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>moderators-signup/">signup form for moderators</a> is open from <?php echo MOD_SIGNUP_START; ?> to <?php echo MOD_SIGNUP_END; ?>.</p>
	    <?php } ?>
	    
	    <?php if ($partic_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>programme/">signup form for participants</a> is open starting <?php echo PARTIC_SIGNUP_START; ?> and you can sign up for any presentation until the presentation ends.</p>
	    <?php } ?>
	    
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
