# SolsticeSchool

This is a web app for managing applications for an online conference,
originally designed for use by [Scholar Social's Solstice School](https://solsticeschool.scholar.social/).

## To set up

1. Create a MySQL database with the tables as specified in
   `schema.sql`

2. Fill in the details for the conference and the credentials for the
   MySQL database in the `config.php` file
   
3. Modify the following `.htaccess` files with an appropriate value
   for `RewriteBase`:
   
   * `archive/.htaccess`
   * `feedback/.htaccess`
   * `moderators-signup/.htaccess`
   * `presenters-signup/.htaccess`
   * `programme/.htaccess`

4. Password-protect the `admin/` folder using an `.htaccess` file that
   refers to a `.htpasswd` file in a folder that is not web-facing
   
Example `.htaccess`:
   
```
AuthType Basic
AuthName "Admin area"
AuthUserFile /path/to/.htpasswd
require valid-user
```

Password-protecting the `admin/` folder is actually very important
otherwise literally anyone can access the admin functions.

5. Enable SMTP mail

First download PHPMailer

```
$ wget https://github.com/PHPMailer/PHPMailer/archive/master.zip
```

Unzip

```
$ unzip master.zip
```

Rename the directory

```
$ mv PHPMailer-master PHPMailer
```

Add the absolute path to the PHPMailer directory to
`config.php`. E.g. if `PHPMailer/` is in `/home/solstice/PHPMailer/`,
the `PHP_MAILER_PATH` should be set to `/home/solstice/`.



## Credit

* Time zones data were taken from: https://github.com/bproctor/timezones
