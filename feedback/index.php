<?php

include_once ("../config.php");

$valid_link = sch_check_feedback_link ($_GET['link']);

include (ABS_PATH . "header.php");

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <?php if ($valid_link) { ?>
		<div class="five-second-removal">
		    <?php

		    if ($_POST['action'] == "send") {
			if (sch_save_feedback ($_POST['link'], $_POST['contact'], $_POST['comments'])) {
			    echo '<div class="alert alert-success" role="alert">Feedback has been successfully sent</div>';
			}
		    }

		    ?>
		</div>
		<h3>Feedback</h3>
		<p>The following form will be sent to the organizers of <?php echo CONF_NAME; ?>, and it is meant to be used to provide feedback on how the conference is run, not on the content of the presentations. Use this form to help us improve the running of the conference and to point out anything that was disruptive to the learning environment we're trying to encourage. Thanks!</p>
		<form action="<?php echo SITE_URL; ?>feedback/<?php echo $_GET['link']; ?>" method="post">
		    <input type="hidden" name="action" value="send">
		    <input type="hidden" name="link" value="<?php echo $_GET['link']; ?>">
		    <div class="form-group mb-3">
			<label for="contact">Your email address</label>
			<input type="email" class="form-control" id="contact" name="contact" aria-describedby="emailHelp" placeholder="Enter email">
			<small id="emailHelp" class="form-text text-muted">This is optional; only provide contact information if you want us to be able to contact you about your comment</small>
		    </div>
		    
		    <div class="form-group" style="margin-bottom: 20px;">
			<label for="comments">Your message to the organizers</label>
			<textarea class="form-control" id="comments" name="comments" rows="5"></textarea>
		    </div>

		    <button class="btn btn-primary btn-lg">Send feedback</button>
		</form>
	    <?php } else { ?>
		<div class="alert alert-danger" role="alert">
		    The feedback link that you followed is either invalid or expired
		</div>
	    <?php } ?>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
